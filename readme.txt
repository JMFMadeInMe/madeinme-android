La parte de la que me he ocupado es la interface para construir el zapato, tanto en Android e iOS. Trabaja con un 'Modelo Dummy' (los datos no son reales, no hay interacci�n con la API).

Trabajo que he hecho:

1.- PASAR EL DISE�O DE 'WEB' AL DE LA 'APP': la disposici�n de lo que yo llamo 'Paleta' es diferente a la de la web. He querido hacerla m�s pr�ctica y adaptada al toque. Tambien he a�adido funcionalidades al zapato de 'toque' para seleccionar distintas partes, y de 'pannig' para mover el angulo del zapato.

2.- PROGRAMACI�N: todo el interface.

3.- MODELO DUMMY: la interface para alimentarse de los datos tira unos m�todos que implementa la clase 'ModelDummy'. Esos m�todos p�blicos deber�n ser los que proporciones el MODELO REAL que se implemente finalmente. De esta el resto del c�digo no se vera afectado con la incorporaci�n de este MODELO REAL que trabaje con la API y el disco. 


Todo lo detallar� cuando prepare la documentaci�n final. La que proporciono ahora  con el c�digo puede no estar actualizada, ya que he ido haciendo cambios y no la he revisado a�n.



Especificaciones Android:

- La he hechos compatible desde la API 8.  
- Trabaja con la librer�a de compatibilidad support-v7.
- En adjuntos mando mi 'workspace' con la app y la librer�a.
- Bitbucket: https://JMFMadeInMe@bitbucket.org/JMFMadeInMe/madeinme-android.git


Especificaciones iOS:

 - En adjuntos mando 'zip' del projecto.
 - Bitbucket: https://JMFMadeInMe@bitbucket.org/JMFMadeInMe/madeinme-ios.git

