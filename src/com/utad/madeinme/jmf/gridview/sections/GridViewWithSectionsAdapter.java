package com.utad.madeinme.jmf.gridview.sections;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.utad.madeinme.R;
import com.utad.madeinme.utils.Utils;

/**
 * Jose Manuel Fierro Conchouso, 2014
 *
 * `Gridview' con 'secciones'
 *
 * *   -------
 *  -> Fuentes
 *     -------
 *     
 *     	Estructura:
 *     		- elaboraci�n propia (JMFierro, 2014)
 *  
 *  
 *  	Implementaci�n del fragment:
 *  		- http://javatechig.com/android/android-gridview-example-building-image-gallery-in-android
 *  		- http://jarroba.com/programar-fragments-fragmentos-en-android/
 *  		- http://stackoverflow.com/questions/9889175/is-it-possible-to-refresh-the-view-of-a-fragment
 *  		- http://vikaskanani.wordpress.com/2011/07/20/android-custom-image-gallery-with-checkbox-in-grid-to-select-multiple/
 *  
 *  
 * 		Secciones en gridview:
 *  		- https://github.com/madhu314/sectionedgridview
 *
 */
public class GridViewWithSectionsAdapter extends BaseAdapter implements
		View.OnClickListener {

	/* ----------
	 *  Listener
	   ----------*/
	public static interface OnClickListener {
		public void onClickItem (String sectionName, int sectionPosition, int position, View v);
//		public void onClickItem (String sectionName, int position, View v);
	}

	private OnClickListener mListener = null;
	
	public void setOnClickListener (OnClickListener listener) {
		mListener = listener;
	}

	private SparseBooleanArray idAnimations = new SparseBooleanArray();
	private int mListItemRowWidth = -1;
	private int mGridItemSize = -1;
	private int mListViewHeight = -1;
	private boolean mIsCurrentPositionVisible;
	private View mOldItem;
	
	/* ------
	 * Datos
	  -------*/
	private LinkedHashMap<String, ArrayList<Bitmap>> mData;
	private ListView mListview;

	private int mNumberOfChildrenInRow = -1;
	private int[] mChildrenSpacing = null;
	private int mChildSpacing = -1;
	private LinkedHashMap<String, Cursor> mSectionCursors = null;
	private LinkedHashMap<String, Integer> mSectionRowsCount = new LinkedHashMap<String, Integer>();
	private Context mContext = null;
	
	private int mPosition;
	private int mSection;

	public static final int VIEW_TYPE_HEADER = 0;
	public static final int VIEW_TYPE_ROW = 1;
//	public static final int MIN_SPACING = 3;
	public static final int CHILD_SPACING = 3;


	public GridViewWithSectionsAdapter(
			Context context,
			LinkedHashMap<String, 
			Cursor> sectionCursors,
			ListView listview,
			int gridItemSquareSize,
			int section,
			int position,
			LinkedHashMap<String,ArrayList<Bitmap>> data) {

		//+++++++++++ JMFfierro ++++++++++
		this.mListview = listview;
		this.mListItemRowWidth = listview.getWidth();  //listItemRowSize;
		this.mListViewHeight = listview.getHeight();  // listViewHeight;
		this.mIsCurrentPositionVisible = false;
		
		this.mSectionCursors = sectionCursors;
		this.mGridItemSize = gridItemSquareSize;

		this.mPosition = position;
		this.mSection = section;
		
		/*--------
		 * Datos
		  --------*/
		this.mData = data; 
				
		// griditem size is always less that list item size
		if (mGridItemSize > this.mListItemRowWidth) {
			throw new IllegalArgumentException(
					mContext.getResources().getString(R.string.app_fatalError_grid));
		}
		
		// calculate items number of items that can fit into row size
		// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		mNumberOfChildrenInRow = mListItemRowWidth / (mGridItemSize + CHILD_SPACING);
		mChildSpacing = CHILD_SPACING;
		int numberOfGaps = mNumberOfChildrenInRow - 1;
		
		// distribute spacing gap equally first
		mChildrenSpacing = new int[numberOfGaps];
		for (int i = 0; i < numberOfGaps; i++) {
			mChildrenSpacing[i] = mChildSpacing;
		}
		// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		
		/**
		mNumberOfChildrenInRow = mListItemRowWidth / mGridItemSize;
		int reminder = mListItemRowWidth % mGridItemSize;
		if (reminder == 0) {
			mNumberOfChildrenInRow = mNumberOfChildrenInRow - 1;
			reminder = mGridItemSize;
		}

		int numberOfGaps = 0;
		int toReduce = 0;
		
		while (mChildSpacing < MIN_SPACING) {
			mNumberOfChildrenInRow = mNumberOfChildrenInRow - toReduce;
			reminder += toReduce * mGridItemSize;
			numberOfGaps = mNumberOfChildrenInRow - 1;
			mChildSpacing = reminder / numberOfGaps;
			toReduce++;
		}

		int spacingReminder = reminder % numberOfGaps;

		// distribute spacing gap equally first
		mChildrenSpacing = new int[numberOfGaps];

		for (int i = 0; i < numberOfGaps; i++) {
			mChildrenSpacing[i] = mChildSpacing;
		}

		// extra reminder distribute from beginning
		for (int i = 0; i < spacingReminder; i++) {
			mChildrenSpacing[i]++;
		}
		*/

		this.mContext = context;

	}

	@Override
	public int getCount() {

		mSectionRowsCount.clear();

		// count is cursors count + sections count
		int sections = mSectionCursors.size();

		int count = sections;
		// count items in each section
		for (String sectionName : mSectionCursors.keySet()) {
			int sectionCount = mSectionCursors.get(sectionName).getCount();
			int numberOfRows = sectionCount / mNumberOfChildrenInRow;
			if (sectionCount % mNumberOfChildrenInRow != 0) {
				numberOfRows++;
			}

			mSectionRowsCount.put(sectionName, numberOfRows);
			count += numberOfRows;
		}

		return count;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = null;
		boolean isSectionheader = isSectionHeader(position);

		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			/*------------------
			 *  Crear una Seccion
			  ------------------*/
			if (isSectionheader) {
				v = inflater.inflate(R.layout.materials_gridviewsections_section_header, null);
			}
			/* ---------------
			 *  Crear un Item
			   ---------------*/
			else {
				LinearLayout ll = (LinearLayout) inflater.inflate(
						R.layout.materials_gridviewsections_list_row, null); 
				v = ll;
				ll = (LinearLayout) ll.findViewById(R.id.gridviewsections_row_item);
				// add childrenCount to this
				for (int i = 0; i < mNumberOfChildrenInRow; i++) {
					// add a child
					View child = inflater.inflate(R.layout.materials_gridviewsections_data_item, null);
//					Utils.checkItemView(mContext, position, child, parent, R.drawable.back_radius10_blacktrans5);
					ll.addView(child, new LinearLayout.LayoutParams(
							mGridItemSize, mGridItemSize));

					if (i < mNumberOfChildrenInRow - 1) {
						// now add space view
						View spaceItem = new View(mContext);
						ll.addView(spaceItem, new LinearLayout.LayoutParams(
								mChildrenSpacing[i], ll.getHeight()));
					}
				}
			}

		} else {
			v = convertView;
		}


		
		/* -----------------------
		 * 
		 * Carga de imagenes 
		 * (s�lo las visibles)
		 * 
		 --------------------------*/
		String sectionName = whichSection(position);
		// +++++++++ por JMFierro ++++++++++++++++++++++++++
		int sectionPosition = whichSectionPosition(position);
		// ++++++++++++++++++++++++++++++++++++++++++++++++++

		if (isSectionheader) {
			TextView tv = (TextView) v;
			tv.setText(sectionName);
		} else {
			LinearLayout ll = (LinearLayout) v;
			LinearLayout rowPanel = (LinearLayout) ll.findViewById(R.id.gridviewsections_row_item);
			/**
			// ++++++++ Anulaci�n division listView +++++++
			View divider = ll.findViewById(R.id.row_item_divider);
			divider.setVisibility(View.VISIBLE);
			// ++++++++++++++++++++++++++++++++++++++++++++
			 */

			// check if this position corresponds to last row
			boolean isLastRowInSection = isLastRowInSection(position);
//			int positionInSectionOfList = positionInSectionOfList(position);
			int positionInSectionOfGrid = mNumberOfChildrenInRow * positionInSectionOfList(position);

			Cursor c = mSectionCursors.get(sectionName);

			// --

			// set all children visible first
			for (int i = 0; i < 2 * mNumberOfChildrenInRow - 1; i++) {
				// we need to hide grid item and gap
				View child = rowPanel.getChildAt(i);
				child.setVisibility(View.VISIBLE);

				// leave alternate
				if (i % 2 == 0) {
					/* ---------------------
					 * 
					 * Texto Item
					 * 
					 * ---------------------*/
					// its not gap
					// +++++++++++++++ Anulaci�n texto +++++++++++++++++++++
					if (c.moveToPosition(positionInSectionOfGrid)) {  	// +
						String dataName = c.getString(0);				// +
						TextView tv = (TextView) child					// +
								.findViewById(R.id.data_item_text);		// +
						tv.setText(dataName);							// +
					}													// +
					// +++++++++++++++++++++++++++++++++++++++++++++++++++++

					
					/* ---------------------
					 * 
					 * Imagen Item
					 * 
					 * ---------------------*/
					ImageView imageView = (ImageView) child
							.findViewById(R.id.data_item_image); 
					ImageView highlight = (ImageView) child
							.findViewById(R.id.imageHighlight); 
//					Utils.checkItemView(mContext, position, imageView, parent, R.drawable.back_radius10_blacktrans5);
//					Utils.checkItemView (parent, v, R.id.imageHighlight);


					
					// ........ Datos: Imagen ..........
					ArrayList<Bitmap> arrayList = new ArrayList<Bitmap>(); 
					arrayList = mData.get(sectionName); 
					int size = arrayList.size();
					if (positionInSectionOfGrid < mData.get(sectionName).size()) {
						Bitmap bm = Utils.BitmapRoundedCorner(mData.get(sectionName).get(positionInSectionOfGrid));
						imageView.setImageBitmap(bm);
						
						// Check
						if (positionInSectionOfGrid == mPosition && sectionPosition == mSection) { 
							highlight.setVisibility(View.VISIBLE);
							mIsCurrentPositionVisible = true;
							mOldItem = highlight;
						}
						
//						imageView.setBackgroundColor(Color.BLUE);  //(R.drawable.selector_off);
					}

					
					/*-------
					 * View
					  -------*/
					ViewHolder holder = new ViewHolder();
					holder.sectionName = sectionName;
					// +++++++++ por JMFierro +++++++++++++++
					holder.sectionPosition = sectionPosition;
					// ++++++++++++++++++++++++++++++++++++++
					holder.positionInSectionOfGrid = positionInSectionOfGrid;
					holder.parent = child;

					imageView.setTag(holder);
					imageView.setOnClickListener(this);

					positionInSectionOfGrid++;

				}
			}

			if (isLastRowInSection) {
				/**
				// ++++++++ Anulaci�n division listView +++++++
				divider.setVisibility(View.INVISIBLE);
				// ++++++++++++++++++++++++++++++++++++++++++++
				 */
				// check how many items needs to be hidden in last row
				int sectionCount = mSectionCursors.get(sectionName).getCount();

				int childrenInLastRow = sectionCount % mNumberOfChildrenInRow;

				if (childrenInLastRow > 0) {
					int gaps = childrenInLastRow - 1;

					for (int i = childrenInLastRow + gaps; i < rowPanel
							.getChildCount(); i++) {
						// we need to hide grid item and gap
						View child = rowPanel.getChildAt(i);
						child.setVisibility(View.INVISIBLE);
					}

				}
			}

		}

		
		if (!mIsCurrentPositionVisible && mPosition != -1)
			mListview.smoothScrollToPosition(position+1);
		
		return v;
	}

	public void scrollVisible() {
		
			mListview.smoothScrollToPosition(7);
			mListview.invalidate();
//			GridviewAdapter adapter = (GridviewAdapter)mGridviewTypes.getAdapter();
//			adapter.notifyDataSetChanged();


	}
	
	private boolean isLastRowInSection(int position) {

		for (String key : mSectionCursors.keySet()) {
			int size = mSectionRowsCount.get(key) + 1;

			if (position == size - 1)
				return true;

			position -= size;
		}

		return false;
	}

	private boolean isSectionHeader(int position) {

		for (String key : mSectionCursors.keySet()) {
			int size = mSectionRowsCount.get(key) + 1;

			if (position == 0)
				return true;

			position -= size;
		}

		return false;

	}

	private String whichSection(int position) {

		for (String key : mSectionCursors.keySet()) {
			int size = mSectionRowsCount.get(key) + 1;

			if (position < size) {
				return key;
			}

			position -= size;
		}

		return null;

	}

	/** .....................................
	 * 
	 *  Devuelve la secci�n.
	 * 
	 *  @author Jose Manuel Fierro Conchouso
	 *  
	 * @param position
	 * @return
	 * 
	 .........................................*/
	private int whichSectionPosition(int position) {

		int sectionPosition = 0;
		for (String key : mSectionCursors.keySet()) {
			int size = mSectionRowsCount.get(key) + 1;

			if (position < size) {
				return sectionPosition;
			}

			sectionPosition++;
			position -= size;
		}

		return -1;

	}

	private int positionInSectionOfList(int position) {

		for (String key : mSectionCursors.keySet()) {
			int size = mSectionRowsCount.get(key) + 1;

			if (position < size) {
				return position - 1;
			}

			position -= size;
		}

		return -1;

	}

	
	@Override
	public int getViewTypeCount() {
		return 2;
	}

	@Override
	public int getItemViewType(int position) {
		if (isSectionHeader(position)) {
			return VIEW_TYPE_HEADER;
		}

		return VIEW_TYPE_ROW;
	}

	@Override
	public boolean isEnabled(int position) {
		// if (isSectionHeader(position)) {
		// return false;
		// }
		//
		// return true;

		return false;

	}

	public int gapBetweenChildrenInRow() {
		return mChildSpacing;
	}

//	public void setListener(OnGridItemClickListener listener) {
//		this.mListener = listener;
//	}

	@Override
	// +++++++++ por JMFierro ++++++++++++++++++++++++++++++++++++++
	public void onClick(View v) {

		mIsCurrentPositionVisible = true;
		
		if (mOldItem != null)
			mOldItem.setVisibility(View.GONE);
		
		ViewHolder holder = (ViewHolder) v.getTag();
//		if (this.mListener != null) {
//			mListener.onGridItemClicked(holder.sectionName,
//					holder.positionInSectionOfGrid, holder.parent);
//		}
		if (mListener != null)
			mListener.onClickItem(holder.sectionName, holder.sectionPosition, 
					holder.positionInSectionOfGrid, holder.parent);
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	}

	public static class ViewHolder {
		String sectionName;
		int sectionPosition;
		int positionInSectionOfGrid;
		View parent;
	}

	// TODO -- cleaning view and click listners and making sure context aint
	// leaked

}
