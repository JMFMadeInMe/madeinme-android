
package com.utad.madeinme.jmf.gridview.sections;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Toast;

import com.utad.madeinme.R;
import com.utad.madeinme.jmf.gridview.GridviewAdapter;

/**
 * Jose Manuel Fierro Conchouso, 2014
 *
 * Pr�ctica MadeInMe
 * Master Desarrollo de apps para smartphone y tablet
 * U-Tad
 * 
 * Visualiza un `Gridview' con 'secciones'.
 * 
 * GridView con secciones:
 * 
 * 		- El 'Gridview' se crea llamando al m�todo publico setupGridviewSections().
 * 
 * 		- Los datos para rellenar el 'Gridview' se pasasn con el parametro data (LinkedHashMap<String, ArrayList<Bitmap>>). 
 * 
 * 		- B�sicamente es un listview que se rellena con las imagenes que entran en una fila. El n�mero de 
 * 		imagenes que entran en cada fila dependen del tama�o que se les haya dado. (El c�digo ha sido adaptado 
 * 		del ejemplo hecho por 'madhu314' p�blicado en 'github')
 * 
 * 		- Listener: mListener.onItemClick (String sectionName, int position, View v); 
 * 					-> callback para avisar de la selecci�n de un item. 
 * 
 * 
 * --------
 * Esquema:
 * --------    
 *  |				|------- JMFGridviewSections.setupGridviewSections()
 *  |						 |
 *  |						 |---- mListener:
 *  |						 |		setOnGridviewSectionsItemClickListener (OnGridviewSectionsItemClickListener listener) 
 *  |						 |      -> mListener.onItemClick (String sectionName, int position, View v);
 *  |						 |
 *  |						 ---- GridViewSectionsAdapter.java
 *  |							 |
 *  |							 |---- public static final int CHILD_SPACING = 3;
 *  |							 |---- materials_gridviewsections_section_header-xml
 *  |							 |---- materials_gridviewsections_list_row.xml (lisview)
 *  |							 |---- gridviewsections_row_item.xml (item gridview)
 * 
 *     
 *     
 * -------
 * Fuentes
 * -------
 *     
 *     	Estructura:
 *     		- elaboraci�n propia (JMFierro, 2014)
 *  
 *  
 *  	Implementaci�n del fragment:
 *  		- http://javatechig.com/android/android-gridview-example-building-image-gallery-in-android
 *  		- http://jarroba.com/programar-fragments-fragmentos-en-android/
 *  		- http://stackoverflow.com/questions/9889175/is-it-possible-to-refresh-the-view-of-a-fragment
 *  		- http://vikaskanani.wordpress.com/2011/07/20/android-custom-image-gallery-with-checkbox-in-grid-to-select-multiple/
 *  
 *  
 * 		Secciones en gridview:
 *  		- https://github.com/madhu314/sectionedgridview
 *  
 */

public class JMFGridviewWithSections {

	/* -----
	 * Datos
	   -----*/
	LinkedHashMap<String, ArrayList<Bitmap>> mSectionsData;
	
	//	public static final String TAG = "GridViewFragment";
	private GridView mGridView;
	private GridviewAdapter mCustomGridAdapter;
	private Context mContex;

	// ++++++++++++++++ Gridview con secciones ++++++++++++++
	private ListView mListView;
	private Dataset mDataSet;
	private GridViewWithSectionsAdapter mSectionsAdapter = null;
	private LinkedHashMap<String, Cursor> mCursorMap;
	private int mPosition;
	private int mSection;
	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++

	
	/* ------------------------
	 * Listener para item click
	   ------------------------*/
	private OnClickListener mListener;
	
	public interface OnClickListener {
		public void onClickItem (String sectionName, int sectionPosition, int position, View v);
	}
	
	public void setOnClickListener (OnClickListener listener) {
		mListener = listener;
	}


	// Constructor vacio
	public JMFGridviewWithSections(){
	}



	public JMFGridviewWithSections(
								Context aContext, 
								View aView,
								int aShoepart, 
								int aTypeOfShoepart, 
								int aShoeSubpart,
								int aMaterialsPosition,
								int aMaterialsSectionPosition,
								LinkedHashMap<String, ArrayList<Bitmap>> data) {
		
		setupGridviewSections(aContext, aView, aMaterialsPosition, aMaterialsSectionPosition, data);

	}


	/** ------------------
	 * Crea le grid View
	 --------------------*/
	/**
	private void setupGridview() {
		mGridView = (GridView) getActivity().findViewById(mCallback.onGetGridviewId(getTag()));
		mCustomGridAdapter = new GridViewAdapter(getActivity(), 
				R.layout.gridview_item, 
				this.getData());
		mGridView.setAdapter(mCustomGridAdapter);

		//  Listener item
		mGridView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				mCallback.onGridviewItemChanged(position, getTag());
				//				Toast.makeText(getActivity(), "" 
				//                        + position, Toast.LENGTH_SHORT).show();
			}
		});
	}
	*/



	public void setupGridviewSections(
			final Context aContext, 
			View aView, 
			LinkedHashMap<String, ArrayList<Bitmap>> data) {
		
		setupGridviewSections(aContext, aView, 0, 0, data);
	
	}

	public void setupGridviewSections(
			final Context aContext, 
			View aView, 
			int position,
			int section,
			LinkedHashMap<String, ArrayList<Bitmap>> data) {

		//		View aView = ((Activity)aContext).getWindow().getDecorView().findViewById(android.R.id.content);

		mContex = aContext;
		mPosition = position;
		mSection = section;
		mDataSet = new Dataset();


		/* ------------------------------------
		 * Carga de secciones: titulo y tama�o
		   ------------------------------------*/
		mSectionsData = data; 
		Iterator iteratorSections = mSectionsData.entrySet().iterator();
		for (Map.Entry<String, ArrayList<Bitmap>> entry : mSectionsData.entrySet()) {
			String key = entry.getKey();
			ArrayList<Bitmap> value = entry.getValue();

			// ----- titulo y tama�o ------
			mDataSet.addSection(key, value.size());
		}		

		mCursorMap = mDataSet.getSectionCursorMap();

		//		mListView = (ListView) getActivity().findViewById(R.id.listview);
		mListView = (ListView) aView.findViewById(R.id.materialsListview);
		updateGrid(aContext);

//		int height = mListView.getHeight();
//		mListView.getViewTreeObserver().addOnGlobalLayoutListener(
//				new OnGlobalLayoutListener() {
//
//					@Override
//					public void onGlobalLayout() {
//						mListView.getViewTreeObserver()
//						.removeGlobalOnLayoutListener(this);
//						
//						updateGrid(aContext);
//					}
//				});

	}


	
	private void updateGrid(Context aContext ){
//		mListView.getViewTreeObserver()
//		.removeGlobalOnLayoutListener(victim);

		// now check the width of the list view
		int width = mListView.getWidth();
		int height = mListView.getHeight();

		//						mSectionsAdapter = new GridViewSectionsAdapter(
		//								getActivity(), mCursorMap, mListView
		//								.getWidth(), mListView.getHeight(),
		//								getResources().getDimensionPixelSize(
		//										R.dimen.grid_item_size), getSectionsData());

		mSectionsAdapter = new GridViewWithSectionsAdapter(
														aContext, 
														mCursorMap, 
														mListView,
														aContext.getResources().getDimensionPixelSize(R.dimen.grid_item_size), 
														mSection,
														mPosition,
														mSectionsData);   //getSectionsData());

		mSectionsAdapter.setOnClickListener(new GridViewWithSectionsAdapter.OnClickListener() {

			@Override
			public void onClickItem(String sectionName, int sectionPosition, int position, View v) {
//				public void onClickItem(String sectionName, int position, View v) {
				Cursor sectionCursor = mCursorMap.get(sectionName);
				if(sectionCursor.moveToPosition(position)) {
					String data = sectionCursor.getString(0);
					String msg = "(JMF)Item clicked is: " + data + "\n(Section#: " + sectionPosition + ")";
					Toast.makeText(mContex, msg, Toast.LENGTH_SHORT).show();
					if (mListener != null) 
						mListener.onClickItem(sectionName, sectionPosition, position, v);
				}

			}
		});
		mListView.setAdapter(mSectionsAdapter);

		mListView.setDividerHeight(mSectionsAdapter
				.gapBetweenChildrenInRow());

	}

}


