
package com.utad.madeinme.jmf.gallery.horizontal;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.utad.madeinme.R;
import com.utad.madeinme.utils.Utils;

/**
 * Jose Manuel Fierro Conchouso, 2014
 *
 * Pr�ctica MadeInMe
 * Master Desarrollo de apps para smartphone y tablet
 * U-Tad
 * 
 * Galeria de view's(ImageView) con scroll horizontal.
 * 
 * XTML:
 * 
 * 		- R.id.gellery_horizontalScroll
 * 		- R.id.gallery
 * 		- R.id.imageContent
 * 
 * Se vale de un 'HorizontalScrollView' que contiene un 'LinearLayout' y utiliza un 'ImageView'
 * 
 *    <HorizontalScrollView
        android:layout_width="match_parent"
        android:layout_height="match_parent" >
        <LinearLayout
            android:id="@+id/mygallery"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:orientation="horizontal"
            >
        </LinearLayout>
    </HorizontalScrollView>

    <ImageView
        android:id="@+id/imageView1"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:src="@drawable/ic_launcher" />

 * -------
 * Esquema
 * ------- 
 * 
 *  |		|		-------- JMFHorizontalScrollView.java
 *  |		|				 |
 *  |		|				 |---- mListener:
 *  |		|				 |		setOnClickItemGalleryListener (OnClickItemGalleryListener listener)
 *  |		|				 |      -> mListener.onItemClick (int position);
 *  |		|				 |
 *  |		|				 |---- idItemLayout Linearlayout
 *  |		|				 |
 *  |		|				 |----	public int mIdHorizontalScroll = R.id.gellery_horizontalScroll;
 *  |		|				 |		public int mIdGallery = R.id.gallery;
 *  |		|				 |		public int mIdImageContent = R.id.imageContent;
 */
public class JMFHorizontalScrollGallery {

	public int mIdHorizontalScroll;
	public int mIdGallery;
	public int mIdImageContent;

	private HorizontalScrollView mHorizontalScroll;
	private LinearLayout mGallery;

	private int mScroll;
	private int mItemCurrent;


	/* -------------------
	 * Listener click item
	   -------------------*/
	private OnClickListener mListener;

	public interface OnClickListener{
		public void onClickItem(int position);
	}

	public void setOnClickListener (OnClickListener listener) {
		mListener = listener;
	}



	public JMFHorizontalScrollGallery() {
		mItemCurrent = -1;
		
		mIdHorizontalScroll = R.id.gallery_horizontalScroll;
		mIdGallery = R.id.gallery;
		mIdImageContent = R.id.imageContent;
		
	}
	public JMFHorizontalScrollGallery(int position) {
		mItemCurrent = position;

		mIdHorizontalScroll = R.id.gallery_horizontalScroll;
		mIdGallery = R.id.gallery;
		mIdImageContent = R.id.imageContent;

	}

	// Sin position
	public JMFHorizontalScrollGallery(
			final Context context,
			final View view, 
			int idItemLayout,
			ArrayList<Bitmap> data) {
	
		this(
			context,
			view, 
			idItemLayout,
			data, 
			-1);
	}
	
	public JMFHorizontalScrollGallery(
			final Context context,
			final View view, 
			int idItemLayout,
			ArrayList<Bitmap> data, 
			int position) {

		this(position);
		//		mCurrentItem = 0;

		showHorizontalViewScroll(
				context, view, idItemLayout, data, position);

	}



	/** ---------------------------------
	 * A�ade items al HorizontalScroll
	    ---------------------------------*/
	public void showHorizontalViewScroll(
			final Context aContext,
			final View aView, 
			int idItemLayout,
			ArrayList<Bitmap> aData, int position) {

		mHorizontalScroll = (HorizontalScrollView) aView.findViewById(mIdHorizontalScroll);
		mGallery = (LinearLayout)aView.findViewById(mIdGallery);
		mGallery.removeAllViews();

		/* ---------------
		 * Vista del item
		   ---------------*/
		LayoutInflater inflater = (LayoutInflater) aContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		LinearLayout viewItem = (LinearLayout) inflater.inflate(idItemLayout, null);
		ImageView imageContentPre = (ImageView) viewItem.findViewById(mIdImageContent);
		final ImageView imageContent = (ImageView) viewItem.findViewById(mIdImageContent);
		
		//		 final ImageView imageHighligh = (ImageView) viewItem.findViewById(R.id.imageHighlight);


		/* ------------------------------------------------
		 *  Recorre todos los items y los a�ade a 'Gallery'. 
		   -----------------------------------------------*/
		if (aData != null)
			for (int i=0; i<aData.size(); i++){


				ImageView imageView = new ImageView(aContext);
				imageView.setScaleType(imageContent.getScaleType());
				imageView.setLayoutParams(imageContent.getLayoutParams());
				
				/*
				 * Separacion entre items
				 */
				setItemSeparation(imageView);
//				imageView.setPadding(2, 0, 2, 0);
				//			imageView.setBackgroundDrawable(aContext.getResources().getDrawable(R.drawable.selector_off));


				// Identifica posicion del item
				imageView.setId(i);
				//		Bitmap bm = scaleCenterCrop(mData.get(i), 70, 70);
				//		imageView.setImageBitmap(bm);

				Bitmap bitmap = Utils.BitmapRoundedCorner(aData.get(i));
				imageView.setImageBitmap(bitmap);	//aData.get(i));

				// Primer item.
				if (i == mItemCurrent) {
					// Guarda �ltimo item seleccionado
					mGallery.setTag(imageView); 
				}

				// Listener para el click de un item.
				imageView.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						checkItem(aContext, v, v.getId());

					}
				});

				/* -------------------------
				 * A�ade imagen a la geleria
			   -------------------------*/
				mGallery.addView(imageView);
			}    

		checkItem(aContext, position);
	}


	/** ...........................................................................
	 * 
	 * Check item.
	 * Llama a OnClickListener de la vista del Item correspondiente a la posici�n, 
	 * (con ello se v� a al checkItem(Context context, View v, int position)
	 *  con todos los par�metros.)
	 * 
	  .............................................................................*/
	private void checkItem(Context context, int position) {

		View v = (View) mGallery.getChildAt(position);
		if (v != null) v.performClick();
		//		checkItem(context, v, position);
	}

	/** ................................................................................................
	 * 
	 * Check item. 
	 * (hay t�mbien una version con s�lo el parametro 'position', que provaca la llamada a este m�todo.)
	  ...................................................................................................*/
	private void checkItem(Context context, View v, int position) {

		if (v != null) {

			// Guarda posicion actual
			mItemCurrent = position;
			//		mCurrentItem = v.getId();

			// Recupera Item anterior seleccinado que fue guardado.
			//		View gallery = (View) v.getParent(); //view = (View) v.getParent();
			View viewLast = (View) mGallery.getTag(); 
			if (viewLast != null) {
				//		viewLast.setBackgroundDrawable(aContext.getResources().getDrawable(R.drawable.selector_off));
				viewLast.setBackgroundColor(Color.TRANSPARENT);
				//			viewLast.setPadding(10, 10, 10, 10);
			}
			mGallery.setTag(v);  // Guarda �ltimo item seleccionado

			// Selecci�n nuevo item
			//					ImageView imageViewHigh = new ImageView(aContext);
			//					imageViewHigh.setScaleType(imageHighligh.getScaleType());
			//					imageViewHigh.setLayoutParams(imageHighligh.getLayoutParams());
			//					imageViewHigh.setVisibility(View.VISIBLE);
			//					imageViewHigh.setBackgroundColor(Color.RED);
			v.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.back_radius10_blacktrans5));
			//		v.setBackgroundDrawable(aContext.getResources().getDrawable(R.drawable.back_radius30_madeinmelight));
			setItemSeparation(v);
//			v.setPadding(5, 5, 5, 5);


			/* --------------------------------------------------
			 * Scroll para el item seleccionado si no es visible
			  ---------------------------------------------------*/
			// Calculo de la posici�n del Item
			int withItemView = mGallery.getHeight();
			//			int withView = v.getWidth();
			int withViewParent = mHorizontalScroll.getWidth();
			int scroll = withViewParent - (withItemView*(mItemCurrent+1));

			// Si el calculo es negativo el item no es visible, es necesario hacer scroll.
			if (scroll < 0) {

				mScroll = Math.abs(scroll);

				// Se asegura que la vista ha sido a sociada a la pantallla.
				mHorizontalScroll.post(new Runnable() {
					public void run() {
						mHorizontalScroll.smoothScrollTo((mScroll), 0);          
					}
				});


			}


			/* ---------
			 * Callback
			  ----------*/
			if(mListener != null) 
				mListener.onClickItem(mItemCurrent);

		}
	}

	private void setItemSeparation (View view){
		view.setPadding(2, 5, 2, 5);

	}

}

