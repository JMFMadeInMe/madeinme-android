package com.utad.madeinme.jmf.model;

/*
 * Jon Ander Rodriguez Hidalgoo, 2014
 *
 * Pr�ctica MadeInMe
 * Master Desarrollo de apps para smartphone y tablet
 * U-Tad
 * 
 */

public class Zapato {
	
	String dTipo;
	String dEleccion;
	String[] arrayDDatos;
	String tTipo;
	String tEleccion;
	String[] arrayTDatos;
	String cTipo;
	String cEleccion;
	String[] arrayCDatos;
	String adTipo;
	String adEleccion;
	String[] arrayAdDatos;
	String atTipo;
	String atEleccion;
	String[] arrayAtDatos;
	String xTipo;
	String xEleccion;
	String[] arrayXDatos;

	public Zapato() {
		// TODO Auto-generated constructor stub
	}

	public Zapato(String dTipo, String dEleccion, String[] arrayDDatos,
			String tTipo, String tEleccion, String[] arrayTDatos, String cTipo,
			String cEleccion, String[] arrayCDatos, String adTipo,
			String adEleccion, String[] arrayAdDatos, String atTipo,
			String atEleccion, String[] arrayAtDatos, String xTipo,
			String xEleccion, String[] arrayXDatos) {
		// TODO Auto-generated constructor stub
				this.dTipo = dTipo;
				this.dEleccion = dEleccion;
				this.arrayDDatos = arrayDDatos;
				this.tTipo = tTipo;
				this.tEleccion = tEleccion;
				this.arrayTDatos =arrayTDatos;
				this.cTipo = cTipo;
				this.cEleccion = cEleccion;
				this.arrayCDatos = arrayCDatos;
				this.adTipo = adTipo;
				this.adEleccion = adEleccion;
				this.arrayAdDatos = arrayAdDatos;
				this.atTipo = atTipo;
				this.atEleccion = atEleccion;
				this.arrayAtDatos = arrayAtDatos;
				this.xTipo = xTipo;
				this.xEleccion = xEleccion;
				this.arrayXDatos = arrayXDatos;
	}

}
