package com.utad.madeinme.jmf.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.widget.Toast;

import com.utad.madeinme.R;
import com.utad.madeinme.api.ApiJMFierroMadeInMe;
import com.utad.madeinme.utils.Utils;
import com.utad.madeinme.utils.UtilsNet;
/**
 * @author Jose Manuel Fierro Conchouso, 2014
 * 
 * @serial
 * Pr�ctica MadeInMe
 * Master Desarrollo de apps para smartphone y tablet
 * U-Tad
 * 
 *  ========
 *   MODELO
 *  ========

 *	 ------
	 Modelo
	 ------

		  El Modelo contiene los datos de un gridView o Gallery, 
	    para ello utiliza un HashMap.
	    
	    	-> private LinkedHashMap<String, ArrayList<Bitmap>> mDataHashMap = new LinkedHashMap<String, ArrayList<Bitmap>>();
	
	    Por ejemplo para el gridview de los 'tipos' en PaletteFragment.java 
	    se crea un objeto:
	    
				- **Tipos :** showTypesOfShoepartGridview(familyIndex,...) -> apiConnect(url...) -> ...
				
				- **Subpars :** showShoeSubpartsHorizontalScroll(familyIndex,...) -> apiConnect(url...) -> ...
				
				
				*apiConnect(url...) de **PaletteFragment.java** ejecuta en un hilo la conexi�n a la api:*
				
				1. -> model.apiConnect(url): 
				2. -> model.getSection(0) :*obtiene un ArrayList de bitmaps.*
				3. -> mHandler.sendMessage(msg): *avisando del fin de la descarga.*
				
				
				*a continuaci�n la secuencia de llamadas sigue asi:*
				
				... -> model.apiConnect(url) -> new ApiJMFierroMadeInMe(url...) 
	
*				Mientras tanto en **PalleteFragment.java** esta a la espera de que la API 
*				termine las descarga de datos, entonces se manda una mensaje a un **Handler 
*				mHandler = new Handler()** con un tag que identifica los datos descargados:* 	 
	 
		mModelMaterials.getShoeRender(
								mShoeFamilyCurrent, 
								shoepartIndex, typeIndex, subpartsIndex, 
								materialsSectionIndex, materialsPositonIndex,  
								sequenceAngle);

		// Mis materiales
		mModelMyMaterials = new ModelDummy(this);
		 
		mModelMyMaterials.addItemFromModel(mModelMaterials, materialsSectionIndex, materialsIndex);

		ArrayList<Bitmap> data = new ArrayList<Bitmap>();
		data = mModelMyMaterials.loadMyMaterials(mShoeFamilyCurrent);


	
		
	----------------------------------
	Uso Modelo en PaletteFragment.java
	----------------------------------
	
		PalleteFragment utiliza el modelo para: Tipos, Subpartes y Materiles:
	
			- **Tipos :** showTypesOfShoepartGridview(familyIndex,...) -> apiConnect(url...) -> ...
			
			- **Subpars :** showShoeSubpartsHorizontalScroll(familyIndex,...) -> apiConnect(url...) -> ...
			
			
			*apiConnect(url...) de **PaletteFragment.java** ejecuta en un hilo la conexi�n a la api:*
			
			1. -> model.apiConnect(url): 
			1. -> model.getSection(0) :*obtiene un ArrayList de bitmaps.*
			1. -> mHandler.sendMessage(msg): *avisando del fin de la descarga.*
			
			
			*a continuaci�n la secuencia de llamadas sigue asi:*
			
			... -> model.apiConnect(url) -> new ApiJMFierroMadeInMe(url...) 
	
		// Materiales
		ModelDummy modelMaterials = new ModelDummy(mContext);
		LinkedHashMap<String, ArrayList<Bitmap>> data = modelMaterials.loadSectionsMaterials(
				aContext,  //getActivity(), 
				shoeFamilyIndex, 
				shoepartIndex, 
				typeOfShoepartIndex,
				subpartIndex);

 *
 * 
 *  ===============================================
 *  * M�todos p�blicos necesarios para la interfaz *
 *  ===============================================
 * 
 * 	** ......................................
	 * Conexi�n a la api.
	  ---------------------------------------*
	public Bitmap apiConnect(String urlString) 

		
    .................................................................
	* Devuelve la imagen renderizada del zapato.
	...................................................................*
	public Bitmap getShoeRender (int familyIndex, int shoeparIndex, int typeIndex,
			int subpartsIndex, int materialsSectionIndex, int materialsPositonIndex, int sequenceAngle) 


	*----------------------------------------
	 * Carga de las Secciones de Materiales.
	  ---------------------------------------*
	public LinkedHashMap<String, ArrayList<Bitmap>> loadSectionsMaterials(Context context, int shoeFamilyIndex, int shoepartIndex, int typeOfShoepartIndex, int shoeSubpartIndex) {
	

	**-------------------------
	 * Carga de mis materiales.
	  -------------------------*
	public ArrayList<Bitmap> loadMyMaterials(int shoeFamilyIndex) {

	
	**.............................................
	 * A�ade item desde otro modelo
	 * (ej.- modelMaterials -> modelMyMaterials)
	 * 
	 * Lo a�ade a la secci�n 0
	  .............................................*
	public void addItemFromModel(ModelDummy modelSource, int sectionIndex, int positionIndex) {
		
	
	**--------
	 * Varios.
	  --------*
	public void addItem(Bitmap itemBitmap, int sectionIndex, int positionIndex) {
	public Bitmap getItem(int sectionIndex, int positionIndex) {
	public ArrayList<Bitmap> getSection (int sectionIndex) {
	public static String getShoepartNameFolder (int position) {
		

 */
public class ModelDummy extends Activity{
	
//	public static String FAMILIA_BAILARINA = "bailarina";
//	public static String FAMILIA_SALON = "salon";
//	public static String FAMILIA_MTACON = "masculinotacon";
//	public static String FAMILIA_MPLANO = "masculinoplano";
//	public static String FAMILIA_SANDALIATACON = "sandaliatacon";
//	public static String FAMILIA_SANDALIAPLANA = "sandaliaplana";
//	public static String FAMILIA_BOTINCORDONES = "botincordones";

	// Familia de zapatos
	public static int FAMILIA_SALON = 0;
	public static int FAMILIA_BAILARINA = 1;
	public static int FAMILIA_SLIPPER = 2;
	public static int FAMILIA_RETRO = 3;
	public static int FAMILIA_SANDALIATACON = 4;
	public static int FAMILIA_SANDALIAPLANA = 5;

//	public static String [] FAMILIA = {
//		"bailarina",
//		"salon",
//		"masculinotacon",
//		"masculinoplano",
//		"sandaliatacon",
//		"sandaliaplana",
//		"botincordones"
//	};

	// Partes del zapato
	public static int SHOEPART_BODY = 0;			//"cc" Cuerpo
	public static int SHOEPART_TOECAP = 1;			//"d";	Puntera
	public static int SHOEPART_BACK = 2;			//"t";	Trasera
	public static int SHOEPART_HEEL = 3;			//"tc"; Tacon
	public static int SHOEPART_ORNAMENT_FRONT = 4;	//"ad"; Adorno delantero
	public static int SHOEPART_ORNAMENT_BACK = 5;	//"at";	Adorno trasero
	public static int SHOEPART_ORNAMENT_TAPE = 6;	//"c";  Adorno cinta
	public static int SHOEPART_EXTRAS = 7;			//"x";  Extras
	
	public static String [] SHOEPARTS = {
		"cc",	// Cuerpo
		"d", 	// Puntera
		"t", 	// Trasera
		"tc",	// Tacon
		"c",	// Adorno cinta
		"ad",	// Adorno delantero
		"at",	// Adorno trasero
		"x"		// Extra
	};
	
	public static int ANGLE_0 = 0;
	public static int ANGLE_45 = 1;
	public static int ANGLE_90 = 2;
	public static int ANGLE_135 = 3;
	public static int ANGLE_180 = 4;
	public static int ANGLE_225 = 5;
	public static int ANGLE_270 = 6;
	public static int ANGLE_315 = 7;

	/* +++++++++++++++++++++++++++++++++++++++++++++
	 *  Se utiliza para los datos DUMMY (assets)
	   +++++++++++++++++++++++++++++++++++++++++++++*/
	public static String SHOEPART_BODY_FOLDER = "cuerpo";
	public static String SHOEPART_TOECAP_FOLDER = "puntera";
	public static String SHOEPART_BACK_FOLDER = "trasera";
	public static String SHOEPART_HEEL_FOLDER = "tacon";
	public static String SHOEPART_ORNAMENT_TAPE_FOLDER = "cintas";  // ** Adornos **
	public static String SHOEPART_ORNAMENT_FRONT_FOLDER = "delanteros";
	public static String SHOEPART_ORNAMENT_BACK_FOLDER = "traseros";
	public static String SHOEPART_EXTRAS_FOLDER = "extras";	// ** Extras **
	
	public final static int DOWNLOAD_TYPES_COMPLETED = 0;			
	public final static int DOWNLOAD_SUBPARTS_COMPLETED = 1;			
	public final static int DOWNLOAD_SHOERENDER_COMPLETED = 10;			

	private Context mContext;

	
	
	
	/* -----
	 * Datos
	   -----*/
	private LinkedHashMap<String, ArrayList<Bitmap>> mDataHashMap = new LinkedHashMap<String, ArrayList<Bitmap>>();

	
	
	
	public ModelDummy(Context context) {

		mContext = context;

//		mDataArrayList = new ArrayList<Bitmap>();
		mDataHashMap = new LinkedHashMap<String, ArrayList<Bitmap>>();

	}
	
	

	/** .......................................
	 * 
	 * Carga de Tipos de una parte del zapato.
	 * 
	  .........................................*/
	/**
	public ArrayList<Bitmap> loadTypesOfShoepart(Context context, int shoeFamilyIndex, int shoePartIndex) {
		return Utils.imagesFromAssets(context, getShoepartNameFolder(shoePartIndex), true);
	}
	*/


	/** ..............................
	 * 
	 * Carga de Subpartes de un Tipo.
	 * 
	  ...............................*/
	/**
	public ArrayList<Bitmap> loadSubpartsOfType(Context context, int shoeFamilyIndex, int shoeparIndex, int typeOfShoepartIndex) {

		String subpart = getShoepartNameFolder(shoeparIndex) + "_subpart" + typeOfShoepartIndex;
		return Utils.imagesFromAssets(context, subpart,true);
	}
	*/

	/** .....................................
	 * 
	 * Carga de las Secciones de Materiales.
	 *  
	  .......................................*/
	public LinkedHashMap<String, ArrayList<Bitmap>> loadSectionsMaterials(Context context, int shoeFamilyIndex, int shoepartIndex, int typeOfShoepartIndex, int shoeSubpartIndex) {

		ArrayList<Bitmap> section = new ArrayList<Bitmap>();

		if (shoeSubpartIndex == 0) {
			section = Utils.imagesFromAssets(context, "materials_ante",true);
			mDataHashMap.put("Ante",section);

			section = Utils.imagesFromAssets(context, "materials_charol",true);
			mDataHashMap.put("Charol",section);
		} 
		else if (shoeSubpartIndex == 1) {

			section = Utils.imagesFromAssets(context, "materials_charol",true);
			mDataHashMap.put("Charol",section);
		} 
		else if (shoeSubpartIndex > 1) {
			section = Utils.imagesFromAssets(context, "materials_charol",true);
			mDataHashMap.put("Charol",section);

			section = Utils.imagesFromAssets(context, "materials_ante",true);
			mDataHashMap.put("Ante",section);
		}

		return mDataHashMap;
	}
	

	/**-------------------------
	 * 
	 * Carga de mis materiales.
	 * 
	  -------------------------*/
	public ArrayList<Bitmap> loadMyMaterials(int shoeFamilyIndex) {

		return mDataHashMap.get("");
	}

	
	/**.............................................
	 * 
	 * A�ade item desde otro modelo
	 * (ej.- modelMaterials -> modelMyMaterials)
	 * 
	 * Lo a�ade a la secci�n 0
	 * 
	  .............................................*/
	public void addItemFromModel(ModelDummy modelSource, int sectionIndex, int positionIndex) {
		
		Bitmap itemBitmap = modelSource.getItem(sectionIndex, positionIndex);
		addItem(itemBitmap, 0, positionIndex);
		
	}
	
	public void addItem(Bitmap itemBitmap, int sectionIndex, int positionIndex) {
		
		ArrayList<Bitmap> itemsArrayList = getSection(sectionIndex);

		itemsArrayList.add(itemBitmap);
		
		List keys = new ArrayList(mDataHashMap.keySet());
		String key;
		if (positionIndex < keys.size())
			key = (String)keys.get(positionIndex);
		else 
			key = "";
		
		mDataHashMap.put(key, itemsArrayList);
	}
	
	public Bitmap getItem(int sectionIndex, int positionIndex) {
		
		ArrayList<Bitmap> sectionsArrayList = getSection(sectionIndex);

		Bitmap itemBitmap = null;
		if (sectionIndex < sectionsArrayList.size())
			itemBitmap = sectionsArrayList.get(positionIndex);
		
		return itemBitmap;
	}
	
	public ArrayList<Bitmap> getSection (int sectionIndex) {
		
		// Acceso por index
		List keys = new ArrayList(mDataHashMap.keySet());
		String key;
		
		ArrayList<Bitmap> sectionArrayList = new ArrayList<Bitmap>();
		if (sectionIndex < keys.size()) {
			key = (String) keys.get(sectionIndex);
//			sectionArrayList = (ArrayList<Bitmap>)keys.get(sectionIndex);
			sectionArrayList = mDataHashMap.get(key);
		}
			
		
		return sectionArrayList;
		
	}
	
	public static String getShoepartNameFolder (int shoePartIndex) {
		
		String shoepartString = null;
		
		if (shoePartIndex == SHOEPART_BODY)
			shoepartString = SHOEPART_BODY_FOLDER;  //"cuerpo";
		else if(shoePartIndex == SHOEPART_TOECAP)
			shoepartString = SHOEPART_TOECAP_FOLDER;  //"puntera";
		else if(shoePartIndex == SHOEPART_BACK)
			shoepartString = SHOEPART_BACK_FOLDER; //"trasera";
		else if(shoePartIndex == SHOEPART_HEEL)
			shoepartString = SHOEPART_HEEL_FOLDER;  //"tacon";
		else if(shoePartIndex == SHOEPART_ORNAMENT_TAPE)  // ** Adornos **
			shoepartString = SHOEPART_ORNAMENT_TAPE_FOLDER;  //"cintas";
		else if(shoePartIndex == SHOEPART_ORNAMENT_FRONT)
			shoepartString = SHOEPART_ORNAMENT_FRONT_FOLDER;  //"delanteros";
		else if(shoePartIndex == SHOEPART_ORNAMENT_BACK)
			shoepartString = SHOEPART_ORNAMENT_BACK_FOLDER;  //"traseros";
		else if(shoePartIndex == SHOEPART_EXTRAS)	// ** Extras **
			shoepartString = SHOEPART_EXTRAS_FOLDER;  //"extra";
		
		return shoepartString;
	}

	
	
	/** .................................................................
	 * 
	 * Devuelve la imagen renderizada del zapato.
	 * 
	  ...................................................................*/
	public Bitmap loadShoeRender (int familyIndex, int shoeparIndex, int typeIndex,
			int subpartsIndex, int materialsSectionIndex, int materialsPositonIndex, int sequenceAngle) {
	
		
		Resources res = mContext.getResources();
		int id = R.drawable.shoerender0;
		
		Bitmap shoeBitmap = null;
		
//		apiConnect("http://hidden-reef-8347.herokuapp.com/api/types_toecap/",sequenceAngle);
		
		if (sequenceAngle == ANGLE_0)
			shoeBitmap = BitmapFactory.decodeResource(mContext.getResources() ,R.drawable.shoerender0);
		else if (sequenceAngle == ANGLE_45)
			shoeBitmap = BitmapFactory.decodeResource(mContext.getResources() ,R.drawable.shoerender45);
		else if (sequenceAngle == ANGLE_90)
			shoeBitmap = BitmapFactory.decodeResource(mContext.getResources() ,R.drawable.shoerender90);
		else if (sequenceAngle == ANGLE_135)
			shoeBitmap = BitmapFactory.decodeResource(mContext.getResources() ,R.drawable.shoerender135);
		else if (sequenceAngle == ANGLE_180)
			shoeBitmap = BitmapFactory.decodeResource(mContext.getResources() ,R.drawable.shoerender180);
		else if (sequenceAngle == ANGLE_225)
			shoeBitmap = BitmapFactory.decodeResource(mContext.getResources() ,R.drawable.shoerender225);
		else if (sequenceAngle == ANGLE_270)
			shoeBitmap = BitmapFactory.decodeResource(mContext.getResources() ,R.drawable.shoerender270);
		else if (sequenceAngle == ANGLE_315)
			shoeBitmap = BitmapFactory.decodeResource(mContext.getResources() ,R.drawable.shoerender315);

		return shoeBitmap;
	}

	
	/** ...........................
	 * 
	 * Conexi�n a la api.
	 * Devuelve si hay conexi�n a internet.
	 * 
	 * @param shoeAngle
	 * @return	 		-> Devuelve si hay conexi�n a internet
	 ...............................*/
	public boolean apiConnect(String urlString) {

//		Bitmap shoeBitmap = null;

		// � Hay conexi�n a internet ?
		boolean isNet = false;
		if (UtilsNet.isNet(mContext)) {
			try {

				isNet = true;
				
				/* --------------------
				 * Descarga de imagenes
			   	   --------------------*/
				// Api conexi�n y descarga.
				//	        	ApiJMFierroMadeInMe api = new ApiJMFierroMadeInMe("http://hidden-reef-8347.herokuapp.com/api/types_toecap/", mContext);
				ApiJMFierroMadeInMe api = new ApiJMFierroMadeInMe(urlString, mContext);

				// Acceso de imagenes descargadas (ArrayList de bitmaps)
				ArrayList<Bitmap> bitmapArrayList = api.getBitmap();

				// Guardando imagenes
				if (mDataHashMap != null && bitmapArrayList != null)
					mDataHashMap.put("", bitmapArrayList);

			} catch (IOException e) {
				e.printStackTrace();
				//			Log.e(getApplication().getPackageCodePath(), "Error pasrse JSON");
				Log.e("class ModelDummy:apiConnect()", "Error pasrse JSON");

			}
		}
		
//		shoeBitmap = null;
//
//		return shoeBitmap;
		return isNet;
		
	}
//	private LinkedHashMap<String, ArrayList<Bitmap>> mDataHashMap = new LinkedHashMap<String, ArrayList<Bitmap>>();
//
//	public LinkedHashMap<String, ArrayList<Bitmap>> getDataHashMap () {
//		return mDataHashMap;
//		
//	}
//	
//	public LinkedHashMap<String, ArrayList<Bitmap>> getDataArrayList (int index) {
//		return mDataHashMap.get(key);
//		
//	}
}