package com.utad.madeinme.utils;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.app.ActionBar.LayoutParams;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.AbsoluteLayout;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.utad.madeinme.R;
//com.utad.madeinme

public class Utils extends Activity {

	public static View rotation(View view, float degrees) {
		// Create an animation instance
		Animation an = new RotateAnimation(0.0f, degrees, 0, 0);

		// Set the animation's parameters
		an.setDuration(1000);               // duration in ms
		an.setRepeatCount(0);                // -1 = infinite repeated
		an.setRepeatMode(Animation.REVERSE); // reverses each repeat
		an.setFillAfter(true);               // keep rotation after animation

		// Aply animation to image view
		view.setAnimation(an);

		return view;
	}


	public static ArrayList<Bitmap> imagesFromAssets(Context context) {
		return imagesFromAssets(context, "",false);
	}

	public static ArrayList<Bitmap> imagesFromAssets(Context context, boolean voidImage) {
		return imagesFromAssets(context, "", voidImage);
	}

	public static ArrayList<Bitmap> imagesFromAssets(Context context, String subfolder) {
		return imagesFromAssets(context, subfolder, false);

	}

	public static ArrayList<Bitmap> imagesFromAssets(Context context, String subfolder, boolean voidImage) {

		String[] f = null; 		
		InputStream is = null;
		ArrayList<Bitmap> bitmapArrayList = new ArrayList<Bitmap>();
		int i=0;
		try { 			
			f = context.getAssets().list(subfolder); 		
		} catch (IOException e) { 			
			// TODO Auto-generated catch block 			
			e.printStackTrace(); 		
		} 
		String path = (subfolder == "" ? "" : subfolder+"/"); 
		for(String fileName:f){
			try {
				is = context.getAssets().open(path + fileName);
				Bitmap bitmap = BitmapFactory.decodeStream(is);
				bitmapArrayList.add(bitmap);  //new ImageItem(bitmap, "Image#" + i));
				i = i++;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if (voidImage)
			if (f.length == 0)
				bitmapArrayList = imagesFromAssets(context, "void");

		return bitmapArrayList;
	}	

	public static Bitmap decodeBitmapFromUri(String path, int reqWidth, int reqHeight) {
		Bitmap bm = null;

		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(path, options);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSize(options, reqWidth, reqHeight);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		bm = BitmapFactory.decodeFile(path, options); 

		return bm;  
	}

	public static int calculateInSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {
			if (width > height) {
				inSampleSize = Math.round((float)height / (float)reqHeight);   
			} else {
				inSampleSize = Math.round((float)width / (float)reqWidth);   
			}   
		}

		return inSampleSize;   
	}

	private static Bitmap overlay(Bitmap bmp1, Bitmap bmp2) {
		Bitmap bmOverlay = Bitmap.createBitmap(bmp1.getWidth(), bmp1.getHeight(), bmp1.getConfig());
		Canvas canvas = new Canvas(bmOverlay);
		canvas.drawBitmap(bmp1, new Matrix(), null);
		canvas.drawBitmap(bmp2, new Matrix(), null);
		return bmOverlay;
	}

	public static void putOverlay(Bitmap bitmap, Bitmap overlay) {
		Canvas canvas = new Canvas(bitmap);
		Paint paint = new Paint(Paint.FILTER_BITMAP_FLAG);
		canvas.drawBitmap(overlay, 0, 0, paint);
	} 

	public static Bitmap scaleCenterCrop(Bitmap source, int newHeight, int newWidth) {
		int sourceWidth = source.getWidth();
		int sourceHeight = source.getHeight();

		// Compute the scaling factors to fit the new height and width, respectively.
		// To cover the final image, the final scaling will be the bigger 
		// of these two.
		float xScale = (float) newWidth / sourceWidth;
		float yScale = (float) newHeight / sourceHeight;
		float scale = Math.max(xScale, yScale);

		// Now get the size of the source bitmap when scaled
		float scaledWidth = scale * sourceWidth;
		float scaledHeight = scale * sourceHeight;

		// Let's find out the upper left coordinates if the scaled bitmap
		// should be centered in the new size give by the parameters
		float left = (newWidth - scaledWidth) / 2;
		float top = (newHeight - scaledHeight) / 2;

		// The target rectangle for the new, scaled version of the source bitmap will now
		// be
		RectF targetRect = new RectF(left, top, left + scaledWidth, top + scaledHeight);

		// Finally, we create a new bitmap of the specified size and draw our new,
		// scaled bitmap onto it.
		Bitmap dest = Bitmap.createBitmap(newWidth, newHeight, source.getConfig());
		Canvas canvas = new Canvas(dest);
		canvas.drawBitmap(source, null, targetRect, null);

		return dest;
	}


	public static int[] toIntArray(ArrayList<Integer> list)  {

		int[] ret = new int[list.size()];
		int i = 0;
		for (Integer e : list)  
			ret[i++] = e.intValue();
		return ret;
	}


	public static void animationMoveView (View v, int newX, int newY) {
		//		// first set the view's location to the end position
		////	view.setLayoutParams(0,100...);  // set to (x, y)
		//	 
		//	// then animate the view translating from (0, 0)
		//	TranslateAnimation ta = new TranslateAnimation(0, 100, 0, 0);
		//	ta.setDuration(1000);
		//	v.startAnimation(ta);

		AbsoluteLayout.LayoutParams absParams = 
				(AbsoluteLayout.LayoutParams)v.getLayoutParams();
		absParams.x = newX;
		absParams.y = newY;
		v.setLayoutParams(absParams);
	}

	public static void animationDownView (View v) {
		//	Animation anim = AnimationUtils.loadAnimation(getActivity(), R.anim.anticipate_overshoot);
		//	gridView.setAnimation(anim);

		AnimationSet set = new AnimationSet(true);

		Animation animation = new AlphaAnimation(0.0f, 1.0f);
		//		animation.setDuration(100);
		//		set.addAnimation(animation);

		animation = new TranslateAnimation(
				Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
				Animation.RELATIVE_TO_SELF, -1.0f, Animation.RELATIVE_TO_SELF, 0.0f
				);
		animation.setDuration(500);
		set.addAnimation(animation);

		v.startAnimation( set );
	}


	public static Bitmap BitmapRoundedCorner(ImageView imageView) {

		BitmapDrawable drawable = (BitmapDrawable) imageView.getDrawable();
		Bitmap bitmap = drawable.getBitmap();

		return BitmapRoundedCorner(bitmap);
	}

	public static Bitmap BitmapRoundedCorner(Bitmap bitmap) {
		Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
				bitmap.getHeight(), Config.ARGB_8888);
		Canvas canvas = new Canvas(output);

		final int color = 0xff424242;
		final Paint paint = new Paint();
		final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
		final RectF rectF = new RectF(rect);
		final float roundPx = 12;

		paint.setAntiAlias(true);
		canvas.drawARGB(0, 0, 0, 0);
		paint.setColor(color);
		canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		canvas.drawBitmap(bitmap, rect, rect, paint);

		return output;
	}



	/** ......................................................................................
	 * 
	 * Para 'check' un item se superpone un 'view' sobre �l. El 'view' que hace de check se le 
	 * reconoce por 'nameId.
	 * 'nameId' es el 'nombre' del id del view (R.id.<nameId>) 
	 * (ej R.id.highlight_item; 'nameId'='highlight_item')
	 * 
	 * En el tag del parent del view eta guardado el �ltimo item 'check' anterior si lo hubiere. 
	 * Se usan dos niveles de parent para asegurase de que en al menos uno esta guardado
	 *
	 * @author Jose Manuel Fierro Conchouso, 2014.
	 *   
	 * @param nameId: es el 'nombre' del id del view (R.id.<nameId>)
	 * (ej R.id.highlight_item; 'nameId'='highlight_item')
	   ........................................................................................*/
	public static View visibilityViewXOR(Context context, String nameId) {

		// 'Enable' view
		int id = context.getResources().getIdentifier(nameId, "id", context.getPackageName());
		View rootView = ((Activity)context).getWindow().getDecorView().findViewById(android.R.id.content); // android
		View view = rootView.findViewById(id);
		view.setVisibility(View.VISIBLE);

		// (I)'Disable' view anterior y guarda actual.
		View parent = (View) view.getParent();
		if (parent != null){
			View viewOld = (View) parent.getTag();
			if (viewOld != null && viewOld != view)
				viewOld.setVisibility(View.INVISIBLE);
			parent.setTag(view);
		} 

		// (II) 'Disable' view anterior y guarda actual.
		View parent2 = (View) view.getParent().getParent();
		if (parent2 != null){
			View viewOld = (View) parent2.getTag();
			if (viewOld != null && viewOld != view)
				viewOld.setVisibility(View.INVISIBLE);
			parent2.setTag(view);
		} 

		return view;
	}

	/**
	private void blur(Bitmap bkg, View view, float radius) {
	    Bitmap overlay = Bitmap.createBitmap(
	        view.getMeasuredWidth(), 
	        view.getMeasuredHeight(), 
	        Bitmap.Config.ARGB_8888);

	    Canvas canvas = new Canvas(overlay);

	    canvas.drawBitmap(bkg, -view.getLeft(), 
	        -view.getTop(), null);

	    RenderScript rs = RenderScript.create(this);

	    Allocation overlayAlloc = Allocation.createFromBitmap(
	        rs, overlay);

	    ScriptIntrinsicBlur blur = ScriptIntrinsicBlur.create(
	        rs, overlayAlloc.getElement());

	    blur.setInput(overlayAlloc);

	    blur.setRadius(radius);

	    blur.forEach(overlayAlloc);

	    overlayAlloc.copyTo(overlay);

	    view.setBackground(new BitmapDrawable(
	        getResources(), overlay));

	    rs.destroy();
	}
	 */


	public static boolean isTablet(Context context) {
		return (context.getResources().getConfiguration().screenLayout
				& Configuration.SCREENLAYOUT_SIZE_MASK)
				>= Configuration.SCREENLAYOUT_SIZE_LARGE;
	}


	/**.............................................................................
	 * 
	 * Devuelve las coordenada en un View dadas las coordenadas en la pantalla.
	 * 
	 * @return: int[] con las coordenada (x,y) en el view.
	 * 
	   ..............................................................................*/
	public static int[] coordinatesFromScreenToView (View v, float xScreen, float yScreen) {

		/* --------------------------------------------
		 * Identifica (x,y) dentro del view del zapato
		   --------------------------------------------*/
		// View zapato
		int withView = v.getWidth();
		int heightView = v.getHeight();

		// View (x,y)
		int[] location = new int[2];
		v.getLocationOnScreen(location);
		int leftMarginView = location[0];
		int topMarginView = location[1];

		int xView = (int) (xScreen - leftMarginView);
		int yView = (int) (yScreen - topMarginView);

		int coordenatesView[] = { xView, yView};

		return coordenatesView;

	}

	// Convert red to transparent in a bitmap
	public static Bitmap makeTransparent(Bitmap bit, int colorToTransperent) {
		int width =  bit.getWidth();
		int height = bit.getHeight();
		Bitmap myBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
		int [] allpixels = new int [ myBitmap.getHeight()*myBitmap.getWidth()];
		bit.getPixels(allpixels, 0, myBitmap.getWidth(), 0, 0, myBitmap.getWidth(),myBitmap.getHeight());
		myBitmap.setPixels(allpixels, 0, width, 0, 0, width, height);

		for(int i =0; i<myBitmap.getHeight()*myBitmap.getWidth();i++){
			if( allpixels[i] == colorToTransperent)

				allpixels[i] = Color.alpha(Color.TRANSPARENT);
		}

		myBitmap.setPixels(allpixels, 0, myBitmap.getWidth(), 0, 0, myBitmap.getWidth(), myBitmap.getHeight());
		Log.d("Supports alpha?", myBitmap.hasAlpha() + "");
		return myBitmap;
	}


	public static Bitmap drawableToBitmap (Drawable drawable) {
		if (drawable instanceof BitmapDrawable) {
			return ((BitmapDrawable)drawable).getBitmap();
		}

		Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Config.ARGB_8888);
		Canvas canvas = new Canvas(bitmap); 
		drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
		drawable.draw(canvas);

		return bitmap;
	}

	public static int dipToPixels(Context context, int dipValue){ 
		Resources r = context.getResources();
		int px = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dipValue,   r.getDisplayMetrics());
		return px; 
	}

	public static void hideAllChildren(View v){
		if(v instanceof ViewGroup) {
			int count = ((ViewGroup)v).getChildCount();
			for(int k = 0 ; k < count ; k++) {
				hideAllChildren(((ViewGroup)v).getChildAt(k));
			}
			v.setVisibility(View.GONE);
		}
		else {
			v.setClickable(false);
			v.setVisibility(View.GONE);
		}
	}

	public static void showAllChildren(View v){
		if(v instanceof ViewGroup) {
			int count = ((ViewGroup)v).getChildCount();
			for(int k = 0 ; k < count ; k++) {
				showAllChildren(((ViewGroup)v).getChildAt(k));
			}
			v.setVisibility(View.VISIBLE);
		}
		else {
			v.setClickable(true);
			v.setVisibility(View.VISIBLE);
		}
	}

	public static void sendViewToBack(final View child) {
		final ViewGroup parent = (ViewGroup)child.getParent();
		if (null != parent) {
			parent.removeView(child);
			parent.addView(child, 0);
		}
	}


	/** ....................................................................
	 * 
	 * Highlight On/Off item
	 * 
	 *  recupera la posici�n actual por medio de un 'id' y getTag() del 'parent' de la 'vista'.
	 *  , nota!: esto ser�a mejor pasarlo como par�metros al constructor!.
	 *  
	   ....................................................................*/
	public static void checkItemView (Context context, int position, View itemView, ViewGroup parent, int idDrawer) {

		// secci�n
		if (parent.getTag(R.id.id_categoty) != null) {
			int categoryCurrent = (Integer) parent.getTag(R.id.id_categoty); 

			// posici�n
			if (parent.getTag(R.id.id_positions_currentsarray) != null) {
				int currentPositions[] = (int[]) parent.getTag(R.id.id_positions_currentsarray);
				int itemCurrentPosition = currentPositions[categoryCurrent];

				// Check On
				if (position == itemCurrentPosition) 
					itemView.setBackgroundDrawable(context.getResources().getDrawable(idDrawer));

				// Check Off
				else 
					itemView.setBackgroundColor(Color.TRANSPARENT);
			}

		}

		//		notifyDataSetChanged();

	}


	/** ..................................................
	 * 
	 * Selecciona un item, lo guarda y deselecciona el item 
	 * anteriormente. 
	 * Se utiliza el 'tag' en el 'parent' del 'view' 'item'.
	 * 
	 * @param v:			-> vista del item.
	 * @param parent:		-> padre de la vista.
	 * @param idRecourse:	-> Highlight - id para el Highlight que se har� visible.
	 * 
	  ....................................................*/
	public static void checkItemView (View parent, View v, int idRecourse) {


		// Deselecciona el item anteriormente seleccionado
		if (parent.getTag() != null) {
			View itemViewOld = (View) parent.getTag();
			if (itemViewOld != null)
				itemViewOld.findViewById(idRecourse).setVisibility(View.GONE);
		}

		// hace visible el highlight
		if (v != null)
			v.findViewById(idRecourse).setVisibility(View.VISIBLE);

		// Guarda vista del item
		parent.setTag(v);

	}






	/** ...........................................................
	 * 
	 * Recupera el view del item activo
	 * 
	 * -> Recupera posici�n guardada
	 * y llama a checkItem(category, v, position) 
	 * con todos los par�metros, para seleccionar item, 
	 * 
	   ............................................................*/
	public static View getItemView (View parentView) {   //, GridView gridview) {

		if (parentView !=null) { 
			if (parentView.getTag(R.id.id_item_view) !=null) {
				//				GridView gridView = (GridView) mViewPalette.findViewById(R.id.gridview_types);
				View v = ((ViewGroup) parentView).getChildAt(0);

				//				checkTypesOfShoepart(category, v, item);

				return v;
			}
		}

		return null;
	}

	public int getItemPosition (View parent) {

		int itemCurrentPosition = -1;

		/*
		 *  Posici�n y Secci�n
		 */
		if (parent.getTag(R.id.id_categoty) != null) {
			int categoryCurrent = (Integer) parent.getTag(R.id.id_categoty); 

			// posici�n
			if (parent.getTag(R.id.id_positions_currentsarray) != null) {
				int currentPositions[] = (int[]) parent.getTag(R.id.id_positions_currentsarray);
				itemCurrentPosition = currentPositions[categoryCurrent];

			}
		}

		//		/*
		//		 * Posici�n s�lo
		//		 */
		//		else if (parent.getTag(R.id.id_postion) != null) {
		//			itemCurrentPosition = (int[]) parent.getTag(R.id.id_positions_currentsarray);


		return itemCurrentPosition;

	}

	/**
	 * Setup a new 3D rotation on the container view.
	 *
	 * @param position the item that was clicked to show a picture, or -1 to show the list
	 * @param start the start angle at which the rotation must begin
	 * @param end the end angle of the rotation
	 */
	public static void applyRotation(View containerView, float start, float end) {
		// Find the center of the container
		final float centerX = containerView.getWidth() / 2.0f;
		final float centerY = containerView.getHeight() / 2.0f;

		// Animación para visulizar imagen.
		final Rotate3dAnimation rotation =
				new Rotate3dAnimation(start, end, centerX, centerY, 310.0f, true);
		rotation.setDuration(500);
		rotation.setFillAfter(true);
		rotation.setInterpolator(new AccelerateInterpolator());
		//        rotation.setAnimationListener(new DisplayNextView(position));

		containerView.startAnimation(rotation);
	}



	public static void moveViewWithAnimation(View view, float x, float y) {

		if (view != null) {
			// buscando el centro del view
			int width = view.getWidth();
			int height = view.getHeight();
			float xView = x - width/2;
			float yView = y - height/2;

			TranslateAnimation anim=new TranslateAnimation(0, xView, 0, yView);
			anim.setFillAfter(true);
			anim.setDuration(0);
			view.startAnimation(anim);
		}
	}

	public static void moveViewWithMargin(View view, int x, int y) {
		
		if (view != null) {
			// buscando el centro del view
			int width = view.getWidth();
			int height = view.getHeight();
			int xView = x - width/2;
			int yView = y - height/2;
			
			setMarginView(view, xView, yView, 0, 0);
			
		}
	}
	
	public static void moveViewWithMargin(Context context, View view, int x, int y) {
		
		if (view != null) {
			// buscando el centro del view
			int width = view.getWidth();
			int height = view.getHeight();
			int xView = x - dipToPixels(context, width/2);
			int yView = y - dipToPixels(context, height/2);
			
			setMarginView(view, xView, yView, 0, 0);
			
		}
	}


	public static void setMarginView(View view, int left, int top, int right, int bottom) {
		
		FrameLayout.LayoutParams llp = new FrameLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

		llp.setMargins(left, top, right, bottom);
		view.setLayoutParams(llp);

	}

	public static Bitmap getBitmapFromURL(String src) {
	    try {
	        URL url = new URL(src);
	        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
	        connection.setDoInput(true);
	        connection.connect();
	        InputStream input = connection.getInputStream();
	        Bitmap myBitmap = BitmapFactory.decodeStream(input);
	        return myBitmap;
	    } catch (IOException e) {
	        // Log exception
	        return null;
	    }
	}
	
	public static Drawable loadImageFromURL(String url, String name) {
	    try {
	        InputStream is = (InputStream) new URL(url).getContent();
	        Drawable d = Drawable.createFromStream(is, name);
	        return d;
	    } catch (Exception e) {
	        return null;
	    }
	}

}