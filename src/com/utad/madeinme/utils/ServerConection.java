package com.utad.madeinme.utils;
/*
 * Jon Ander Rodriguez Hidalgoo, 2014
 *
 * Pr�ctica MadeInMe
 * Master Desarrollo de apps para smartphone y tablet
 * U-Tad
 * 
 */

import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.utad.madeinme.jmf.model.Zapato;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;

public class ServerConection {

	public ServerConection() {
		// TODO Auto-generated constructor stub
	}
	
	public void jgetDatosZapato(String URLapi, Context mContext){
		ConnectivityManager connManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = connManager.getActiveNetworkInfo();

		if (info != null && info.isConnected()) {
			jgetDatosZapatoAsyncTask asyncTast = new jgetDatosZapatoAsyncTask();
			asyncTast.execute(URLapi);
		}else{
			//error de conexion
		}
	}
	
	private class jgetDatosZapatoAsyncTask extends AsyncTask<String, Void, Zapato> {

		@Override
		protected Zapato doInBackground(String... urls) {
			HttpUtils http = new HttpUtils(urls[0]);
			
			String dTipo = null;
			String dEleccion = null;
			String[] arrayDDatos = null;
			String tTipo = null;
			String tEleccion = null;
			String[] arrayTDatos = null;
			String cTipo = null;
			String cEleccion = null;
			String[] arrayCDatos = null;
			String adTipo = null;
			String adEleccion = null;
			String[] arrayAdDatos = null;
			String atTipo = null;
			String atEleccion = null;
			String[] arrayAtDatos = null;
			String xTipo = null;
			String xEleccion = null;
			String[] arrayXDatos = null;
	
			try {
				http.connect();
	            // Obteniendo datos del json
				// Obteniendo temperatura
				JSONObject httpJson = http.getJSONObject();
				
				int codigoReseponse = httpJson.getInt("code");
				
				switch(codigoReseponse) { // Eleige la opcion acorde al numero de mes
				case 0://OK
					JSONObject current_zapato = httpJson.getJSONObject("response").getJSONObject("opt");
		            JSONObject d = current_zapato.getJSONObject("d");
		            dTipo = String.valueOf(d.getString("type"));
		            dEleccion = String.valueOf(d.getString("choose"));
		            JSONArray dDatos = d.getJSONArray("data");
		            arrayDDatos = new String[dDatos.length()];
		            for(int x = 0; x < dDatos.length(); x = x+1) {
		            	arrayDDatos[x] = dDatos.getString(x);
		             }
		            
		            JSONObject t = current_zapato.getJSONObject("t");
		            tTipo = String.valueOf(t.getString("type"));
		            tEleccion = String.valueOf(t.getString("choose"));
		            JSONArray tDatos = t.getJSONArray("data");
		            arrayTDatos = new String[tDatos.length()];
		            for(int x = 0; x < tDatos.length(); x = x+1) {
		                arrayTDatos[x] = tDatos.getString(x);
		             }
		            JSONObject c = current_zapato.getJSONObject("c");
		            cTipo = String.valueOf(c.getString("type"));
		            cEleccion = String.valueOf(c.getString("choose"));
		            JSONArray cDatos = c.getJSONArray("data");
		            arrayCDatos = new String[cDatos.length()];
		            for(int x = 0; x < cDatos.length(); x = x+1) {
		                arrayCDatos[x] = cDatos.getString(x);
		             }
		            JSONObject ad = current_zapato.getJSONObject("ad");
		            adTipo = String.valueOf(ad.getString("type"));
		            adEleccion = String.valueOf(ad.getString("choose"));
		            JSONArray adDatos = ad.getJSONArray("data");
		            arrayAdDatos = new String[adDatos.length()];
		            for(int x = 0; x < adDatos.length(); x = x+1) {
		            	arrayAdDatos[x] = adDatos.getString(x);
		             }
		            JSONObject at = current_zapato.getJSONObject("at");
		            atTipo = String.valueOf(at.getString("type"));
		            atEleccion = String.valueOf(at.getString("choose"));
		            JSONArray atDatos = at.getJSONArray("data");
		            arrayAtDatos = new String[atDatos.length()];
		            for(int x = 0; x < atDatos.length(); x = x+1) {
		                arrayAtDatos[x] = atDatos.getString(x);
		             }
		            JSONObject x = current_zapato.getJSONObject("x");
		            xTipo = String.valueOf(x.getString("type"));
		            xEleccion = String.valueOf(x.getString("choose"));
		            JSONArray xDatos = x.getJSONArray("data");
		            arrayXDatos = new String[xDatos.length()];
		            for(int y = 0; y < xDatos.length(); y = y+1) {
		                arrayXDatos[y] = xDatos.getString(y);
		             }
				break;
				case 1://ERROR_MISSING_OR_INVALID_PARAMETER
					
				break;
				case 2://ERROR_INTERNAL_ERROR
					
				break;
				case 3://INVALID_STATE
					
				break;
				default://codigo desconocido
					
				break;
				}

	        } catch (JSONException e) {
	            e.printStackTrace();
			} catch (IOException e) {
			} finally {
				http.close();
			}
			//rellenar modelo
			Zapato mZapato = new Zapato( dTipo,
										 dEleccion,
										 arrayDDatos,
										 tTipo,
										 tEleccion,
										 arrayTDatos,
										 cTipo,
										 cEleccion,
										 arrayCDatos,
										 adTipo,
										 adEleccion,
										 arrayAdDatos,
										 atTipo,
										 atEleccion,
										 arrayAtDatos,
										 xTipo,
										 xEleccion,
										 arrayXDatos);
			return mZapato;
		}
		
		@Override
		protected void onPostExecute(Zapato zapato) {
			
			
	    }
	}
    

}
