package com.utad.madeinme.utils; 

import android.view.View;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

/**
* Jose Manuel Fierro Conchouso, 2014.
* 
* Allows an ObjectAnimator to set/get margins of a view
*
* fuente: https://gist.github.com/openback/3850853
*/
public class MarginProxy {
	private View mView;

	public MarginProxy(View view) {
		mView = view;
	}

	public void setWidth(int width) {
		MarginLayoutParams lp = (MarginLayoutParams) mView.getLayoutParams();
		lp.width=width;
		//	LinearLayout.LayoutParams params = button.getLayoutParams();
		//	params.width = 100;
		//	button.setLayoutParams(params);
		mView.requestLayout();
	}


	public void setHeight(int height) {
		MarginLayoutParams lp = (MarginLayoutParams) mView.getLayoutParams();
		lp.height=height;
		//	LinearLayout.LayoutParams params = button.getLayoutParams();
		//	params.width = 100;
		//	button.setLayoutParams(params);
		mView.requestLayout();
	}

	public int getLeftMargin() {
		MarginLayoutParams lp = (MarginLayoutParams) mView.getLayoutParams();
		return lp.leftMargin;
	}

	public void setLeftMargin(int margin) {
		MarginLayoutParams lp = (MarginLayoutParams) mView.getLayoutParams();
		lp.setMargins(margin, lp.topMargin, lp.rightMargin, lp.bottomMargin);
		mView.requestLayout();
	}

	public int getTopMargin() {
		MarginLayoutParams lp = (MarginLayoutParams) mView.getLayoutParams();
		return lp.topMargin;
	}

	public void setTopMargin(int margin) {
		MarginLayoutParams lp = (MarginLayoutParams) mView.getLayoutParams();
		lp.setMargins(lp.leftMargin, margin, lp.rightMargin, lp.bottomMargin);
		mView.requestLayout();
	}

	public int getRightMargin() {
		MarginLayoutParams lp = (MarginLayoutParams) mView.getLayoutParams();
		return lp.rightMargin;
	}

	public void setRightMargin(int margin) {
		MarginLayoutParams lp = (MarginLayoutParams) mView.getLayoutParams();
		lp.setMargins(lp.leftMargin, lp.topMargin, margin, lp.bottomMargin);
		mView.requestLayout();
	}

	public int getBottomMargin() {
		MarginLayoutParams lp = (MarginLayoutParams) mView.getLayoutParams();
		return lp.bottomMargin;
	}

	public void setBottomMargin(int margin) {
		MarginLayoutParams lp = (MarginLayoutParams) mView.getLayoutParams();
		lp.setMargins(lp.leftMargin, lp.topMargin, lp.rightMargin, margin);
		mView.requestLayout();
	}
}