package com.utad.madeinme.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.utad.madeinme.R;
import com.utad.madeinme.jmf.model.ModelDummy;

/**
 * Jose Manuel Fierro Conchouso, 2014
 *
 * Segmento de pesta�as con raya.
 * 
 */
public class PaletteSegmentTabsFragment extends Fragment {
	
	private View mView;
	
	/*
	 * Callback
	 */
	private OnClickListener mListener;
	public interface OnClickListener {
		public void onItemClick(int position);
	}
	public void setOnClickListener (OnClickListener listener) {
		mListener = listener;
	}
	
	
	
	public PaletteSegmentTabsFragment () {
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			ViewGroup container, Bundle savedInstanceState) {
		
		mView = inflater.inflate(R.layout.segmenttabs_radiogroup, container, false);
		
		setupRadioGroupTextListener(R.id.text_button0, R.id.text_viewLine0, ModelDummy.SHOEPART_BODY);
		setupRadioGroupTextListener(R.id.text_button1, R.id.text_viewLine1, ModelDummy.SHOEPART_TOECAP);
		setupRadioGroupTextListener(R.id.text_button2, R.id.text_viewLine2, ModelDummy.SHOEPART_BACK);
		setupRadioGroupTextListener(R.id.text_button3, R.id.text_viewLine3, ModelDummy.SHOEPART_HEEL);
//		setupRadioGroupTextListener(R.id.text_button4, R.id.text_viewLine4, ModelDummy.SHOEPART_);
//		setupRadioGroupTextListener(R.id.text_button5, R.id.text_viewLine5, _GOODIES);
		
		return mView;
	}
	
	/* .-------------------------------------------------------
	 * public void setupRadioGroupTextListener (int txtId, int viewId, int position)
	 * 
	 * Listener para los click sobre el 'RadioGroup txt' del Pageview.
	 * @param txtId  (R.id.texto)
	 * @param viewId (raya del selctor)
	   --------------------------------------------------------*/
	public void setupRadioGroupTextListener (int txtRId, int viewRId, int position) {

		TextView textView = (TextView) mView.findViewById(txtRId);
		final View viewline = (View) mView.findViewById(viewRId); 

		/*
		 *  Listener
		 */
		if (textView != null && viewline != null) {

			Viewholder viewholder = new Viewholder();
			viewholder.position = position;
			textView.setTag(viewholder);

			textView.setOnClickListener( new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					Viewholder viewholder = new Viewholder();
					viewholder = (Viewholder)v.getTag();

					setCheck(viewholder.position);
					if (mListener != null) mListener.onItemClick(viewholder.position);
				}
			});
		}
	}

	
	/*  ------------------------------------------
	 * public void setRadioGroupTextEnable (int position)
	 * Activa el 'RadioGroup text' por posici�n.
	 * @param position
	   -------------------------------------------*/
	public void setCheck (int position) {

		RadioGroup radioGroup = (RadioGroup) mView.findViewById(R.id.text_radioGroup);
		int numSegment = radioGroup.getChildCount();
		
		setRadioGroupTextAllState(View.INVISIBLE);
		if (position>=0 && position<numSegment) 
			setRadioGroupTextState(position, View.VISIBLE);
	}



	/* --------------------------------------------------------------------
	 * public void setRadioGroupTextAllState (int state)
	 * Set al 'state' de todos los 'View'(lineas) de los 'RadioGroup txt'.    
	 * @param state: visible/invisible/gone
	   --------------------------------------------------------------------*/
	public void setRadioGroupTextAllState (int state) {
		setRadioGroupTextState(0, state);
		setRadioGroupTextState(1, state);
		setRadioGroupTextState(2, state);
		setRadioGroupTextState(3, state);
		setRadioGroupTextState(4, state);
		setRadioGroupTextState(5, state);
	}

	/* ------------------------------------------------------------------------
	 * public void setRadioGroupTextState (int position, int state)
	 * Cambia el estado del view' del 'RadioGroup text' se�alado por la posicion. 
	 * @param position:		0,1,2,...
	 * @param state:		visible/invisible/gone
	  -------------------------------------------------------------------------*/
	public void setRadioGroupTextState (int position, int state) {

		if (mView != null) {
			int viewId = getRadioGroupTextIdView(position); //getResources().getIdentifier("viewLine"+position, "view", getPackageName());
			View view = (View) mView.findViewById(viewId); 
			if (view != null) 
				view.setVisibility(state);
		}
	}

	/* -------------------------------------------------------------------------------
	 * public int getRadioGroupTextIdView (int position)
	 * Devuelve el 'id' del 'view' del 'RadioGroup text' correspondiente a la posici�n  
	 * @param position:	0,1,2,...
	 * @return:			'id' del 'view' (ej R.id.viewLine1)
	  --------------------------------------------------------------------------------*/
	public int getRadioGroupTextIdView (int position) {

		//		RadioGroup rg = (RadioGroup) findViewById(R.id.text_radioGroup1);
		//		View vChild = rg.getChildAt(position);
		//		View vSubChild = ((LinearLayout)vChild).getChildAt(1);
		//		int idRadioButton = vSubChild.getId();
		//
		//		return idRadioButton;

		if (position == 0)
			return R.id.text_viewLine0;
		else if (position == 1)
			return R.id.text_viewLine1;
		else if (position == 2)
			return R.id.text_viewLine2;
		else if (position == 3)
			return R.id.text_viewLine3;
		else if (position == 4)
			return R.id.text_viewLine4;
		else if (position == 5)
			return R.id.text_viewLine5;
		else
			return 0;
	}
	
	

	/** @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	 * 
	 *  CLASE Un holder para usaar en RadioGroup text
	 	@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/
	public class Viewholder {
		int position;
	}

}
