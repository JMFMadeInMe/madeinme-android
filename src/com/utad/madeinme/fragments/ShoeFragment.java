/**
 * Jose Manuel Fierro Conchouso, 2014
 *
 * Pr�ctica MadeInMe
 * Master Desarrollo de apps para smartphone y tablet
 * U-Tad
 * 
 * 
 * ----------
 * Callbacks
 * ----------
 * 
 * 		public void onClickShoe(int shoepartIndex);
		public Bitmap onGetShoeRender(int sequenceAngle);
		public Bitmap onGetShoeRender(int shoeparIndex, int typeIndex, int subpartsIndex, 
				int materialsSectionIndex, int materialsPositonIndex, int sequenceAngle);

 *
 * 	-------------------
	 Atributos p�blicos
	-------------------
	public int mShoeAngleCurrent;

 *
 * ----------------
 * M�todos p�blicos
 * ----------------
 *
    // Cambia a la siguiente secuencia en la imagen del zapato.
	public void moveShoe(int direction)

	// Actualizan imagen zapato

 * ... hacen llamada callback -> mListener.onGetShoeRender()
	public void updateShoeView ()
	public void updateShoeView (int sequenceAngle)
	public void updateShoeView (int shoeparIndex, int typeIndex, int subpartsIndex, 
			int materialsSectionIndex, int materialsPositonIndex, int sequenceAngle)

 * ... sin llamada callback 
	public void updateShoeView (Bitmap shoeBitmap)


 * 
 */

package com.utad.madeinme.fragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ImageView;

import com.utad.madeinme.R;
import com.utad.madeinme.jmf.model.ModelDummy;
import com.utad.madeinme.utils.Utils;

public class ShoeFragment extends Fragment {

	/* --------------------
	 * Listener shoe
	  ---------------------*/
	private OnListener mListener;

	public interface OnListener {
		public void onClickShoe(int shoepartIndex);
		
		// Selector de la parte del zapato sobre la imagee del zapato
		public void onClickSelector(float xScreen, float yScreen); 
		public void onClickSelectorVisibility(boolean visibility); 

		public Bitmap onGetShoeRender(int sequenceAngle);
		public Bitmap onGetShoeRender(int shoeparIndex, int typeIndex, int subpartsIndex, 
				int materialsSectionIndex, int materialsPositonIndex, int sequenceAngle);

	}

	public void setOnListener (OnListener listener) {
		this.mListener = listener;
	}

	/*-------------------
	 * Atributos p�blicos
	 *-------------------*/
	public int mShoeAngleCurrent;


	/*-------------------
	 * Atributos privados
	 *-------------------*/
	private int DIRECTION_PANNING_DCHA = 1;
	private int DIRECTION_PANNING_IZQ = -1;

	private Context mContext;

	private ImageView mShoeView;
//	private View mSelectorView;

	private float mTouchRawXPositionInitial;
	private float mTouchRawYPositionInitial;


	public ShoeFragment () {

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mShoeAngleCurrent = ModelDummy.ANGLE_0;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View v = inflater.inflate(R.layout.shoe, container, false);

//		mSelectorView = (View) v.findViewById(R.id.selector);

		mShoeView = (ImageView) v.findViewById(R.id.shoeImageView);
		mShoeView.setOnTouchListener(new View.OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {

				shoeTouchListener(v, event);
				return true;
			}
		});

		return v;
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();

	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		this.mContext = activity;

	}


	/** .....................................
	 * 
	 *  Paninnig y click sobre el zapato
	 *  
	 * @param v	View imagen del zapato.
	 * @param event	MotionEvent evento touch
	 * @author Jose Manuel Fierro, 2014.
	 * 
	  .......................................*/
	private void shoeTouchListener(View v, MotionEvent event) {

		boolean panning = false;
		int sectionsXYViewPrecision = 8;  	// 8x8
		int sectionsXYView = 3;			 	// Se utiliza para identificar areas m�s grandes.


		int index = event.getActionIndex();
		int action = event.getActionMasked();

		float xScreen = event.getRawX(); 
		float yScreen = event.getRawY();

		/* --------------------------------------------
		 * Identifica (x,y) dentro del view del zapato
		   --------------------------------------------*/
		// View zapato
		int withView = v.getWidth();
		int heightView = v.getHeight();

		int coordinatesView[] = Utils.coordinatesFromScreenToView(v, xScreen, yScreen);
		int xView = (int) coordinatesView[0];
		int yView = (int) coordinatesView[1];

		switch(action) {

		case MotionEvent.ACTION_DOWN:

			// Inicializa posicion inicial.
			mTouchRawXPositionInitial = event.getRawX();
			mTouchRawYPositionInitial = event.getRawY();

			break;


			/* --------
			 * Panning
			   --------*/
		case MotionEvent.ACTION_MOVE:
			//			Log.d("","MotionEvent.ACTION_MOVE");

			int sensorPanning = Utils.isTablet(mContext) ? 100 : 85;
			boolean dchaViewPosition = xView > withView/2; 
			boolean topViewPosition = yView < heightView/3; 

			/** ------------ EJE X ------------------
			 * 
			 *  Si se a desplazado el dedo (Panning)
			 *  
			   --------------------------------------*/
			if (Math.abs(mTouchRawXPositionInitial - xScreen) > sensorPanning) {

				panning = true;
//				unSelected();
				if (mListener != null)
					mListener.onClickSelectorVisibility(false);
				/* -------------------------
				 *  Panning de dcha a izq.
				 ---------------------------*/
				if (mTouchRawXPositionInitial > xScreen) {
					if (!topViewPosition)
						moveShoe(DIRECTION_PANNING_IZQ);
					else
						moveShoe(DIRECTION_PANNING_DCHA);
				}
				/* -----------------------
				 *  Panning de izq a dcha.
				  ------------------------*/
				else {
					if (!topViewPosition)
						moveShoe(DIRECTION_PANNING_DCHA);
					else
						moveShoe(DIRECTION_PANNING_IZQ);
				}

				// Actualizacion
				mTouchRawXPositionInitial = xScreen;
			}

			/** ------------ EJE Y -----------------
			 * 				
			 *  Si se a desplazado el dedo (Panning)
			 *  
			   -------------------------------------*/
			if (Math.abs(mTouchRawYPositionInitial - yScreen) > sensorPanning 
					|| 
					(Math.abs(mTouchRawXPositionInitial - xScreen) > sensorPanning/2
							&& Math.abs(mTouchRawYPositionInitial - yScreen) > sensorPanning/2)) {

				panning = true;
//				unSelected();
				if (mListener != null)
					mListener.onClickSelectorVisibility(false);

				/* ------------------
				 *  De abajo a arriba.
				   ------------------*/
				if (mTouchRawYPositionInitial > yScreen) {
					if (!dchaViewPosition)
						moveShoe(DIRECTION_PANNING_IZQ);
					else
						moveShoe(DIRECTION_PANNING_DCHA);
				}

				/* ------------------
				 *  De arriba a abajo.
				   ------------------*/
				else {
					if (!dchaViewPosition)
						moveShoe(DIRECTION_PANNING_DCHA);
					else
						moveShoe(DIRECTION_PANNING_IZQ);
				}

				// Actualizacion
				mTouchRawYPositionInitial = yScreen;
			}

			break;

			/* -----
			 * Click
			  ------*/
		case MotionEvent.ACTION_UP:
			//			Log.d("","MotionEvent.ACTION_UP");

			/** -----------------
			 * 
			 * Click (no panning)
			 * 
			   -----------------*/
			// Si menos del 10 por 1000.
			int dif = (int) (1000*((mTouchRawXPositionInitial/xScreen)-1));
			if (!panning && Math.abs(dif) < 10) { // if (mRawXOld == rawX) {


				/* -----------------------------------
				 * Identifica seccion pulsada.
				   -----------------------------------*/
				// Columna y fila con 'n' secciones = sectionsXYViewPrecision.
				int colViewPrecision = xView / (withView/sectionsXYViewPrecision); 
				int rowViewPrecision = yView / (heightView/sectionsXYViewPrecision);
				// Columna y fila con 'n' secciones = sectionsXYView
				int colView = (sectionsXYView*colViewPrecision)/sectionsXYViewPrecision + 1;
				int rowView = (sectionsXYView*rowViewPrecision)/sectionsXYViewPrecision + 1;


				//				Log.d("","------------------");
				//				Log.d("","ANGLE: " +mShoeAngleCurrent);
				//				Log.d("","------------------");
				//				Log.d("","colView1: " +colViewPrecision);
				//				Log.d("","rowView1: " +rowViewPrecision);


				// Secci�n pulsada
				int shoepartIndex = getShoepartTouch(v, mShoeAngleCurrent, xScreen, yScreen);
				if ( shoepartIndex != -1) {

					// Selecciona la parte del zapato tocada
//					Utils.moveViewWithMargin(mSelectorView, xView, yView); 
					if (mListener != null)
						mListener.onClickSelector(xScreen, yScreen);
//					selected();
					if (mListener != null)
						mListener.onClickSelectorVisibility(true);

					// Actualiza paleta
					if (mListener != null) 
						mListener.onClickShoe(shoepartIndex);
					//					updateTypesOfShoepart(shoepartIndex);
					//					if (rowView >=1 && rowView <=3 && colView >=1 && colView <=3)
					//						Utils.visibilityViewXOR(v.getContext(), "shoeSelectorTrazoImageView" +colView +rowView);
				}

			}

		case MotionEvent.ACTION_CANCEL:
			//			Log.d("","MotionEvent.ACTION_CANCEL");
			break;
		}

	}



	/** ......................................................
	 * 
	 *  Cambia a la siguiente secuencia en la imagen del zapato.
	 *  
	 *  @param direction	int (DIRECTION_PANNING_DCHA � DIRECTION_PANNING_IZQ)
	 * @author Jose Manuel Fierro, 2014.
	 	.....................................................*/
	public void moveShoe(int direction) {

		int sequenceFirstNum = ModelDummy.ANGLE_0;
		int sequenceLastNum = ModelDummy.ANGLE_315;

		if (direction == DIRECTION_PANNING_DCHA)
			mShoeAngleCurrent = mShoeAngleCurrent == sequenceFirstNum ? sequenceLastNum : mShoeAngleCurrent-1;
		else
			mShoeAngleCurrent = mShoeAngleCurrent ==  sequenceLastNum ? sequenceFirstNum : mShoeAngleCurrent+1;

		updateShoeView(mShoeAngleCurrent);


	} 


	public void updateShoeView () {
		updateShoeView (mShoeAngleCurrent);
	}


	public void updateShoeView (int sequenceAngle) {

		Bitmap shoeBitmap = null;
		if (mListener != null)
			shoeBitmap = mListener.onGetShoeRender(sequenceAngle);
//			mListener.onGetShoeRender(sequenceAngle);

		updateShoeView(shoeBitmap);
	}

	public void updateShoeView (int shoeparIndex, int typeIndex, int subpartsIndex, 
			int materialsSectionIndex, int materialsPositonIndex, int sequenceAngle) {

		Bitmap shoeBitmap = null;
		if (mListener != null)
			shoeBitmap = mListener.onGetShoeRender(shoeparIndex, typeIndex, subpartsIndex, 
					materialsSectionIndex, materialsPositonIndex, sequenceAngle);
//			mListener.onGetShoeRender(shoeparIndex, typeIndex, subpartsIndex, 
//					materialsSectionIndex, materialsPositonIndex, sequenceAngle);

		updateShoeView(shoeBitmap);
	}

	public void updateShoeView (Bitmap shoeBitmap) {

		mShoeView.setImageBitmap(shoeBitmap);
		mShoeView.invalidate();

	}





	/** .................................................................
	 * 
	 * Devuelve la parte del zapato tocada.
	 * 
	 * La secci�n en la que se pulsa se calcula a partir de un n�mero
	 * prefijado de secciones.
	 * 
	 * 
	 * @param v				vista del zapato
	 * @param shoeAngle		angulo del zapato (int ModelDummy.ANGLE_0, ANGLE_45, ...)
	 * @param xScreen		coordenada 'x' del click en pantalla.
	 * @param yScreen		coordenada 'y' del click en pantalla.
	 * 
	 * @return	int shoePart touch
	 * @author Jose Manuel Fierro, 2014.
	 *   
	 ..........................................................................*/
	private int getShoepartTouch(View v, int shoeAngle, float xScreen, float yScreen) {

		/* ---------------------------------------------------------------------------------
		 * Identifica (x,y) dentro del view del zapato.
		 * �Rehago c�lculo para asegurarme que se utilizan int sectionsXYViewPrecision = 8! 
		   ---------------------------------------------------------------------------------*/
		int sectionsXYViewPrecision = 8;

		// View zapato
		int withView = v.getWidth();
		int heightView = v.getHeight();

		// View (x,y)
		int coordinatesView[] = Utils.coordinatesFromScreenToView(v, xScreen, yScreen);
		int xView = (int) coordinatesView[0];
		int yView = (int) coordinatesView[1];

		// View (col,row) - Columna y fila calculadas a partir de (x,y) y el n�mero de secciones en que se divide el view.
		int colView = xView / (withView/sectionsXYViewPrecision); 
		int rowView = yView / (heightView/sectionsXYViewPrecision);


		/* -----------------------------------------------------------
		 *  SECCION CLICK
		 *  �Secciones calculada con int sectionsXYViewPrecision = 3!
		 *  Orden( col, fila)
		   -----------------------------------------------------------*/
		int shoepartIndex = getShoepartTouch(shoeAngle, colView, rowView);

		return shoepartIndex;

	}

	//	private int getShoepart(int int shoeAngle, int colView, int rowView) {
	//		return getShoepart(shoeAngle, colView, rowView);
	//	}


	/** ....................................................................
	 * 
	 * Devuelve la parte del zapato tocada.
	 * 
	 * La secci�n en la que se pulsa se calcula a partir de un n�mero
	 * prefijado de secciones.
	 * 
	 * @author Jose Manuel Fierro, 2014.
	 * 
	 * @param v				vista del zapato
	 * @param shoeAngle		angulo del zapato (int ModelDummy.ANGLE_0, ANGLE_45, ...)
	 * @param colView		columna tocada en la imagen.
	 * @param rowView		fila de la imagen.
	 * 
	 * @return	int shoePart touch
	 * 
	 ..........................................................................*/
	private int getShoepartTouch(int shoeAngle, int colView, int rowView) {

		int shoepartIndex;
		if (shoeAngle ==  ModelDummy.ANGLE_0) 
			shoepartIndex = getShoepartTouchAngle0(colView, rowView);
		else if (shoeAngle ==  ModelDummy.ANGLE_45) 
			shoepartIndex = getShoepartTouchAngle45(colView, rowView);
		else if (shoeAngle ==  ModelDummy.ANGLE_90) 
			shoepartIndex = getShoepartTouchAngle90(colView, rowView);
		else if (shoeAngle ==  ModelDummy.ANGLE_135) 
			shoepartIndex = getShoepartTouchAngle135(colView, rowView);
		else if (shoeAngle ==  ModelDummy.ANGLE_180) 
			shoepartIndex = getShoepartTouchAngle180(colView, rowView);
		else if (shoeAngle ==  ModelDummy.ANGLE_225) 
			shoepartIndex = getShoepartTouchAngle225(colView, rowView);
		else if (shoeAngle ==  ModelDummy.ANGLE_270) 
			shoepartIndex = getShoepartTouchAngle270(colView, rowView);
		else if (shoeAngle ==  ModelDummy.ANGLE_315) 
			shoepartIndex = getShoepartTouchAngle315(colView, rowView);
		else 
			shoepartIndex = -1;

		return shoepartIndex;
	}

	private int getShoepartTouchAngle0(int colView, int rowView) {

		int shoepartIndex;

		if (colView >=3 && colView <=5 && rowView >=3 && rowView <=5 ) { 
			shoepartIndex = ModelDummy.SHOEPART_BODY;
		}
		else if (colView >=3 && colView <=4 && rowView >=3 && rowView <=6 ) { 
			shoepartIndex = ModelDummy.SHOEPART_BODY;
		}
		else if (colView >=1 && colView <=2 && rowView >=6 ) { 
			shoepartIndex = ModelDummy.SHOEPART_TOECAP;
		}
		else if (colView >=6 && colView <=7 && rowView <=1 ) {
			shoepartIndex = ModelDummy.SHOEPART_BACK;
		}
		else if (colView >=6 && rowView <=1) {
			shoepartIndex = ModelDummy.SHOEPART_BACK;
		}
		else if (colView >=6 && rowView <=2) {
			shoepartIndex = ModelDummy.SHOEPART_HEEL;
		}
		else if (colView >=6 && colView <=7 && rowView >=2 && rowView <=5) {
			shoepartIndex = ModelDummy.SHOEPART_HEEL;
		}

		else
			shoepartIndex = -1;

		return shoepartIndex;
	}

	private int getShoepartTouchAngle45(int colView, int rowView) {


		int shoepartIndex;

		if (colView >=5 && colView <=5 && rowView >=3 && rowView <=5 ) { 
			shoepartIndex = ModelDummy.SHOEPART_BODY;
		}
		else if (colView >=5 && colView <=6 && rowView >=3 && rowView <=5 ) { 
			shoepartIndex = ModelDummy.SHOEPART_HEEL;
		}
		else if (colView >=2 && colView <=4 && rowView >=3 && rowView <=6 ) { 
			shoepartIndex = ModelDummy.SHOEPART_BODY;
		}
		else if (colView <=1 && rowView >=5 ) { 
			shoepartIndex = ModelDummy.SHOEPART_TOECAP;
		}
		else if (colView >=6 && rowView <=2 ) {
			shoepartIndex = ModelDummy.SHOEPART_BACK;
		}
		else if (colView >=6 && rowView <=3 ) {
			shoepartIndex = ModelDummy.SHOEPART_HEEL;
		}
		else if (colView >=6 && rowView >=4 ) {
			shoepartIndex = ModelDummy.SHOEPART_HEEL;
		}
		else
			shoepartIndex = -1;

		return shoepartIndex;
	}

	private int getShoepartTouchAngle90(int colView, int rowView) {


		int shoepartIndex;
		if (colView >=3 && colView <=4 && rowView >=1 && rowView <=4 ) { 
			shoepartIndex = ModelDummy.SHOEPART_BODY;
		}
		else if (colView >=2 && colView <=3 && rowView >=4 && rowView <=6 ) { 
			shoepartIndex = ModelDummy.SHOEPART_BODY;
		}
		else if (colView >=1 && colView <=2 && rowView >= 3 && rowView <=5 ) { 
			shoepartIndex = ModelDummy.SHOEPART_TOECAP;
		}
		else if (colView >=5 && colView <=6 && rowView <=2 ) { 
			shoepartIndex = ModelDummy.SHOEPART_BACK;
		}
		else if (colView >=5 && colView <=6 && rowView >=3 ) { 
			shoepartIndex = ModelDummy.SHOEPART_HEEL;
		}
		else
			shoepartIndex = -1;

		return shoepartIndex;
	}

	private int getShoepartTouchAngle135(int colView, int rowView) {


		int shoepartIndex;
		if (colView >=2 && colView <=4 && rowView <=3) { 
			shoepartIndex = ModelDummy.SHOEPART_BACK;
		}
		else if (colView >=2 && colView <=4 && rowView >=4) { 
			shoepartIndex = ModelDummy.SHOEPART_HEEL;
		}
		else
			shoepartIndex = -1;

		return shoepartIndex;
	}

	private int getShoepartTouchAngle180(int colView, int rowView) {


		int shoepartIndex;
		if (colView >=3 && colView <=3 && rowView >=2 && rowView <=4 ) { 
			shoepartIndex = ModelDummy.SHOEPART_BODY;
		}
		else if (colView >=4 && colView <=5 && rowView >=3 && rowView <=6 ) { 
			shoepartIndex = ModelDummy.SHOEPART_BODY;
		}
		else if (colView >=6 && colView <=7 && rowView >=4) { 
			shoepartIndex = ModelDummy.SHOEPART_TOECAP;
		}
		else if (colView <=2 && rowView <=3 ) { 
			shoepartIndex = ModelDummy.SHOEPART_BACK;
		}
		else if (colView <=2 && rowView >=4 ) { 
			shoepartIndex = ModelDummy.SHOEPART_HEEL;
		}
		else
			shoepartIndex = -1;

		return shoepartIndex;
	}


	private int getShoepartTouchAngle225(int colView, int rowView) {


		int shoepartIndex;

		if (colView >=2 && colView <=3 && rowView >=2 && rowView <=4 ) { 
			shoepartIndex = ModelDummy.SHOEPART_BODY;
		}
		else if (colView >=4 && colView <=5 && rowView >=5 && rowView <=6 ) { 
			shoepartIndex = ModelDummy.SHOEPART_BODY;
		}
		else if (colView >=6 && rowView >=5 ) { 
			shoepartIndex = ModelDummy.SHOEPART_TOECAP;
		}

		else if (colView <=2 && rowView <=2 ) { 
			shoepartIndex = ModelDummy.SHOEPART_BACK;
		}
		else if (colView <=2 && rowView <=3 ) { 
			shoepartIndex = ModelDummy.SHOEPART_HEEL;
		}
		else if (colView <=1 && rowView >=4 ) { 
			shoepartIndex = ModelDummy.SHOEPART_HEEL;
		}
		else
			shoepartIndex = -1;

		return shoepartIndex;
	}

	private int getShoepartTouchAngle270(int colView, int rowView) {


		int shoepartIndex;
		if (colView >=3 && colView <=3 && rowView >=2 && rowView <=4 ) { 
			shoepartIndex = ModelDummy.SHOEPART_BODY;
		}
		else if (colView ==3 && colView <=4 && rowView >=3 && rowView <=6 ) { 
			shoepartIndex = ModelDummy.SHOEPART_BODY;
		}
		else if (colView >=3 && colView <=4 && rowView >=3 && rowView <=6 ) { 
			shoepartIndex = ModelDummy.SHOEPART_BODY;
		}
		else if (colView ==3 && rowView ==5 ) { 
			shoepartIndex = ModelDummy.SHOEPART_BODY;
		}
		else if (colView ==2 && rowView ==2 ) { 
			shoepartIndex = ModelDummy.SHOEPART_HEEL;
		}
		else if (colView <=1 && rowView <=2) { 
			shoepartIndex = ModelDummy.SHOEPART_BACK;
		}
		else if (colView <=2 && rowView <=1) { 
			shoepartIndex = ModelDummy.SHOEPART_BACK;
		}
		else if (colView >=5 && colView <=6 && rowView >=5) { 
			shoepartIndex = ModelDummy.SHOEPART_TOECAP;
		}
		else if (colView ==2 && rowView ==4) { 
			shoepartIndex = ModelDummy.SHOEPART_HEEL;
		}
		else
			shoepartIndex = -1;

		return shoepartIndex;
	}

	private int getShoepartTouchAngle315(int colView, int rowView) {


		int shoepartIndex;
		if (colView >=3 && colView <=4 && rowView >=5) { 
			shoepartIndex = ModelDummy.SHOEPART_TOECAP;
		}
		else
			shoepartIndex = -1;

		return shoepartIndex;
	}

//	public void selected() {
//
//		if (mSelectorView != null) 
//			mSelectorView.setVisibility(View.VISIBLE);
//
//	}
//
//	public void unSelected() {
//
//		if (mSelectorView != null) {
//
//			Animation animation = mSelectorView.getAnimation();
//			if (animation != null) {
//				animation.cancel();
//				mSelectorView.setAnimation(null);
//			}
//			mSelectorView.setVisibility(View.GONE);
//		}
//	}
}