/*
 * Jose Manuel Fierro Conchouso, 2014
 *
 * Pr�ctica MadeInMe
 * Master Desarrollo de apps para smartphone y tablet
 * U-Tad
 * 
 */

package com.utad.madeinme;


import java.util.ArrayList;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.ImageView;

import com.utad.madeinme.jmf.model.ModelDummy;
import com.utad.madeinme.utils.Utils;

public class MainActivity extends ActionBarActivity {

	private View mFamilyView;
	private ArrayList<Bitmap> mBitmapArrayList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (Utils.isTablet(getApplicationContext()))
			setContentView(R.layout.activity_main);
		else
			setContentView(R.layout.activity_main_movil);

		ActionBar bar = getSupportActionBar();
		bar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.MadeInMeDarck)));
		//		bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#50000000")));

		//		// Hide the status bar
		//		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

		// Hide the action bar
		getSupportActionBar().hide();

		ActionBar actionBar = getSupportActionBar();
		actionBar.setHomeButtonEnabled(false);

		mFamilyView = (View) findViewById(R.id.family_layout);

		ImageView mainImagenView = (ImageView) findViewById(R.id.mainImageView);
		mainImagenView.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {

				mFamilyView.setVisibility(View.VISIBLE);
			}
		});


		setupListenerFamily(R.id.family_salon, ModelDummy.FAMILIA_SALON);
		setupListenerFamily(R.id.family_bailarina, ModelDummy.FAMILIA_BAILARINA);
		setupListenerFamily(R.id.family_slipper, ModelDummy.FAMILIA_SLIPPER);
		setupListenerFamily(R.id.family_retro, ModelDummy.FAMILIA_RETRO);
		setupListenerFamily(R.id.family_sandalia_tacon, ModelDummy.FAMILIA_SANDALIATACON);
		setupListenerFamily(R.id.family_sandalia_plana, ModelDummy.FAMILIA_SANDALIAPLANA);



	}



	private void designActivity(int shoeFamilyIndex) {

		Intent intent = new Intent(getApplicationContext(), DesignActivity.class);
		intent.putExtra(DesignActivity.ARG_FAMILY, shoeFamilyIndex);
		startActivity(intent);

	}

	private void setupListenerFamily(int idFamilyView, int shoeFamilyIndex) {

		View familySalonView  = (View)  findViewById(idFamilyView);
		familySalonView.setTag(shoeFamilyIndex);

		familySalonView.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				mFamilyView.setVisibility(View.GONE);

				if (v.getTag() != null){
					int shoeFamilyIndex = (Integer) v.getTag();
					designActivity(shoeFamilyIndex);
				}

			};
		});


		
		
	}

}




//public class MainActivity extends ActionBarActivity implements TabsBtnFragment.OnSetTabsBtnFragment {
//	
//
//
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.design_activity);
//		
//		ActionBar bar = getSupportActionBar();
//		bar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.MadeInMeDarck)));
////		bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#50000000")));
//				
////		// Hide the status bar
////		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
////		
////		// Hide the action bar
////        getSupportActionBar().hide();
//		
//		
////	    if (Build.VERSION.SDK_INT < 16)
////	    {
////	        // Hide the status bar
////	        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
////	        // Hide the action bar
////	        getSupportActionBar().hide();
////	    }
////	    else
////	    {
////	        // Hide the status bar
////	        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN);
////	        // Hide the action bar
////	        getActionBar().hide();
////	    }
//		
//	}
//
//	
//	
//	@Override
//	public void onTabsBtnUpdatePosition(int position) {
//		// ------------------------------
//		// Actualizar posicion 'palette'
//		// ------------------------------
//		PaletteFragment palette = (PaletteFragment) getSupportFragmentManager().findFragmentById(R.id.palette_fragment);
//		palette.onUpdatePosition(position);
//	 
//	}
//
//    public void onTabsUpDownToggleClicked(View view) {
//        // Is the toggle on?
//        boolean on = ((ToggleButton) view).isChecked();
//        
//        if (on) {
//            // Enable vibrate
//        } else {
//            // Disable vibrate
//        }
//    }
//
//}

//import android.app.ActionBar;
//import android.app.Activity;
//import android.os.Bundle;
//
//import com.utad.jmf.madeinme.R;

